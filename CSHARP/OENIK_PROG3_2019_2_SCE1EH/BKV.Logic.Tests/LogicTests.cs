﻿// <copyright file="LogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BKV.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;
    using BKV.Logic;
    using BKV.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Contains the tests for all the logic operations.
    /// </summary>
    public class LogicTests
    {
        private Mock<IDriverRepository> mockDrivers;
        private DriverLogic driverLogic;
        private List<drivers> listDrivers;

        private Mock<ICoordsRepo> mockCoords;
        private CoordLogic coordLogic;
        private List<coordsAndDispatchers> listCoords;

        private Mock<IStationRepository> mockStations;
        private StationLogic stationLogic;
        private List<stations> listStations;

        private Mock<IVehiclesRepository> mockVehicles;
        private VehicleLogic vehicleLogic;
        private List<vehicles> listVehicles;

        private Mock<IInspectorRepository> mockInspectors;
        private InspectorLogic inspectorLogic;
        private List<inspectors> listInspectors;

        /// <summary>
        /// The mocktest setup in one obese function.
        /// </summary>
        [SetUp]
        public void SetUpForTesting()
        {
            // DriverSetup
            this.mockDrivers = new Mock<IDriverRepository>();
            this.driverLogic = new DriverLogic(this.mockDrivers.Object);
            this.listDrivers = new List<drivers>()
            {
                new drivers()
                {
                    driver_id = 100,
                    driver_name = "bb",
                    driver_age = 25,
                    driver_hiredDate = DateTime.Now,
                    driver_avgHoursPerWeek = 40,
                    driver_hoursWorkedTotal = 40,
                    driver_lastDrivenVehicleId = 1,
                },

                new drivers()
                {
                    driver_id = 101,
                    driver_name = "bb",
                    driver_age = 25,
                    driver_hiredDate = DateTime.Now,
                    driver_avgHoursPerWeek = 40,
                    driver_hoursWorkedTotal = 40,
                    driver_lastDrivenVehicleId = 2,
                },

                new drivers()
                {
                    driver_id = 108,
                    driver_name = "bb",
                    driver_age = 27,
                    driver_hiredDate = DateTime.Now,
                    driver_avgHoursPerWeek = 40,
                    driver_hoursWorkedTotal = 40,
                    driver_lastDrivenVehicleId = 3,
                },
            };
            this.mockDrivers.Setup(x => x.GetAll()).Returns(this.listDrivers.AsQueryable());
            this.mockDrivers.Setup(x => x.GetOne(It.IsAny<int>())).Returns((int id) => this.listDrivers.Where(x => x.driver_id == id).FirstOrDefault());
            this.mockDrivers.Setup(x => x.DeleteDriver(It.IsAny<int>())).Callback((int i) =>
                {
                    drivers myDriver = this.listDrivers.Where(x => x.driver_id == i).FirstOrDefault();
                    this.listDrivers.Remove(myDriver);
                });
            this.mockDrivers.Setup(x => x.UpdateDriver(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())).Callback(
                (int id_, string name_, int age_, DateTime hiredDate_, int avgHoursPerWeek_, int hoursWorkedTotal_, int lastDrivenVehicleId_) =>
                {
                    var i = this.listDrivers.FindIndex(x => x.driver_id == id_);
                    if (i != -1)
                    {
                        this.listDrivers[i] = new drivers()
                        {
                            driver_id = id_,
                            driver_name = name_,
                            driver_age = age_,
                            driver_hiredDate = hiredDate_,
                            driver_avgHoursPerWeek = avgHoursPerWeek_,
                            driver_hoursWorkedTotal = hoursWorkedTotal_,
                            driver_lastDrivenVehicleId = lastDrivenVehicleId_,
                        };
                    }
                });
            this.mockDrivers.Setup(x => x.CreateDriver(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())).Callback(
                (int id_, string name_, int age_, DateTime hiredDate_, int avgHoursPerWeek_, int hoursWorkedTotal_, int lastDrivenVehicleId_) =>
                {
                    this.listDrivers.Add(new drivers()
                        {
                        driver_id = id_,
                        driver_name = name_,
                        driver_age = age_,
                        driver_hiredDate = hiredDate_,
                        driver_avgHoursPerWeek = avgHoursPerWeek_,
                        driver_hoursWorkedTotal = hoursWorkedTotal_,
                        driver_lastDrivenVehicleId = lastDrivenVehicleId_,
                        });
                });

            // StationSetup
            this.mockStations = new Mock<IStationRepository>();
            this.stationLogic = new StationLogic(this.mockStations.Object);
            this.listStations = new List<stations>()
            {
                new stations()
                {
                    station_id = 1,
                    station_name = "Újpest-Központ",
                    station_personnelAmount = 100,
                    station_cassaAvailability = 1,
                    station_ticketMachineAvailability = 1,
                    station_lastRepairsDate = DateTime.Now.AddYears(-10),
                },
                new stations()
                {
                    station_id = 2,
                    station_name = "Kőbánya-Kispest",
                    station_personnelAmount = 100,
                    station_cassaAvailability = 1,
                    station_ticketMachineAvailability = 1,
                    station_lastRepairsDate = DateTime.Now.AddYears(-10),
                },
            };
            this.mockStations.Setup(x => x.GetAll()).Returns(this.listStations.AsQueryable());

            // CoordSetup
            this.mockCoords = new Mock<ICoordsRepo>();
            this.coordLogic = new CoordLogic(this.mockCoords.Object);
            this.listCoords = new List<coordsAndDispatchers>()
            {
                new coordsAndDispatchers()
                {
                    coord_id = 1,
                    coord_name = "Újpesti Dezső",
                    coord_age = 55,
                    coord_hiredDate = DateTime.Now.AddYears(-20),
                    coord_avgHoursPerWeek = 40,
                    coord_hoursWorkedTotal = 1,
                    coord_stationId = 1, // ID of újpest-központ
                },
            };
            this.mockCoords.Setup(x => x.GetAll()).Returns(this.listCoords.AsQueryable());

            // VehicleSetup
            this.mockVehicles = new Mock<IVehiclesRepository>();
            this.vehicleLogic = new VehicleLogic(this.mockVehicles.Object);
            this.listVehicles = new List<vehicles>()
            {
                new vehicles()
                {
                    vehicle_id = 1,
                    vehicle_typeId = 1,
                    vehicle_serialnum = "ABC-123",
                    vehicle_linenumber = "20E",
                    vehicle_dateManufactured = DateTime.Now.AddYears(-30),
                    vehicle_lastInspectedDate = DateTime.Now.AddYears(-1),
                    vehicle_totalHoursDriven = 5200,
                    vehicle_lastRepairsDate = DateTime.Now.AddYears(-2),
                },
                new vehicles()
                {
                    vehicle_id = 2,
                    vehicle_typeId = 1,
                    vehicle_serialnum = "ABC-555",
                    vehicle_linenumber = "120",
                    vehicle_dateManufactured = DateTime.Now.AddYears(-20),
                    vehicle_lastInspectedDate = DateTime.Now.AddYears(-2),
                    vehicle_totalHoursDriven = 4900,
                    vehicle_lastRepairsDate = DateTime.Now.AddYears(-4),
                },
            };
            this.mockVehicles.Setup(x => x.GetAll()).Returns(this.listVehicles.AsQueryable());

            // InspectorSetup

            this.mockInspectors = new Mock<IInspectorRepository>();
            this.inspectorLogic = new InspectorLogic(this.mockInspectors.Object);
            this.listInspectors = new List<inspectors>()
            {
                new inspectors()
                {
                    inspector_id = 1,
                    inspector_name = "Dezső János",
                    inspector_age = 44,
                    inspector_hiredDate = DateTime.Now.AddYears(-5),
                    inspector_avgHoursPerWeek = 44,
                    inspector_hoursWorkedTotal = 5000,
                    inspector_lastInspectedVehicleId = 1,
                },
                new inspectors()
                {
                    inspector_id = 2,
                    inspector_name = "Balassa János",
                    inspector_age = 44,
                    inspector_hiredDate = DateTime.Now.AddYears(-15),
                    inspector_avgHoursPerWeek = 44,
                    inspector_hoursWorkedTotal = 4000,
                    inspector_lastInspectedVehicleId = 2,
                },
                new inspectors()
                {
                    inspector_id = 3,
                    inspector_name = "Hrutka Pál",
                    inspector_age = 44,
                    inspector_hiredDate = DateTime.Now.AddYears(-2),
                    inspector_avgHoursPerWeek = 44,
                    inspector_hoursWorkedTotal = 4000,
                    inspector_lastInspectedVehicleId = 3,
                },

            };
            this.mockInspectors.Setup(x => x.GetAll()).Returns(this.listInspectors.AsQueryable());
        }

        /// <summary>
        /// This makes sure that getOneDriver returns correct driver.
        /// </summary>
        [Test]
        public void WhenDriverExists_AndAskingForDriverByPK_TheNamesMatch()
        {
            var res = this.driverLogic.GetOneDriver(101);
            Assert.That(res.driver_name, Is.EqualTo("bb"));
            this.mockDrivers.Verify(x => x.GetOne(101), Times.Once);
        }

        /// <summary>
        /// Tests deleting.
        /// </summary>
        [Test]
        public void WhenDriverExists_AndDeletingThatDriver_ItCeasesToExist()
        {
            this.driverLogic.DeleteDriver(100);
            Assert.That(this.driverLogic.GetOneDriver(100), Is.Null);
            this.mockDrivers.Verify(x => x.DeleteDriver(100), Times.Once);
        }

        /// <summary>
        /// Tests that there's no exception when trying to create with duplicate primarykeys.
        /// </summary>
        [Test]
        public void WhenDriverExists_AndCreatingDriverWithDuplicatePrimaryKeys_DoesNotThrowException()
        {
            DateTime time = DateTime.Now;
            Assert.DoesNotThrow(() => this.driverLogic.CreateDriver(100, "bb", 25, time, 40, 40, 1));
            this.mockDrivers.Verify(x => x.CreateDriver(100, "bb", 25, time, 40, 40, 1), Times.Never);
        }

        /// <summary>
        /// Trying to delete non-existing item with a non-existing primary key, doesnt throw exception.
        /// </summary>
        [Test]
        public void WhenDriverDoesntExist_AndTryingToDeleteNonExistingDriver_DoesNotThrowException()
        {
            Assert.DoesNotThrow(() => this.driverLogic.DeleteDriver(200));
            this.mockDrivers.Verify(x => x.DeleteDriver(200), Times.Never);
        }

        /// <summary>
        /// Doesnt throw exception when trying to access non-existing primary key.
        /// </summary>
        [Test]
        public void WhenDriverDoesntExist_AndTryingToAccessThatDriver_DoesNotThrowException()
        {
            Assert.DoesNotThrow(() => this.driverLogic.GetOneDriver(200));
        }

        /// <summary>
        /// Checks to not allow inhuman age numbers.
        /// </summary>
        [Test]
        public void WhenCreatingDriver_AndInputAgeIsInvalid_DriverIsNotCreated()
        {
            DateTime time = DateTime.Now;
            this.driverLogic.CreateDriver(999, "bb", 9999, time, 40, 40, 1);
            this.driverLogic.CreateDriver(1000, "bb", -9999, time, 40, 40, 1);
            Assert.That(this.driverLogic.GetOneDriver(999), Is.Null);
            this.mockDrivers.Verify(x => x.CreateDriver(999, "bb", 9999, time, 40, 40, 1), Times.Never);
            this.mockDrivers.Verify(x => x.CreateDriver(1000, "bb", -9999, time, 40, 40, 1), Times.Never);
        }

        /// <summary>
        /// Doesnt throw exception for trying to update non-existing drivers.
        /// </summary>
        [Test]
        public void WhenDriverDoesNotExist_AndTryingToUpdateThatDriver_DoesNotThrowException()
        {
            DateTime time = DateTime.Now;
            Assert.DoesNotThrow(() => this.driverLogic.UpdateDriver(2000, "bb", 25, time, 40, 40, 1));
            this.mockDrivers.Verify(x => x.UpdateDriver(2000, "bb", 25, time, 40, 40, 1), Times.Never);
        }

        /// <summary>
        /// UPDATE information correct.
        /// </summary>
        [Test]
        public void WhenDriverExists_AndTryingToUpdateThatDriver_UpdatedInformationIsCorrect()
        {
            DateTime time = DateTime.Now;
            this.driverLogic.UpdateDriver(108, "bb", 55, time, 40, 40, 1);
            Assert.That(this.driverLogic.GetOneDriver(108).driver_age, Is.EqualTo(55));
            this.mockDrivers.Verify(x => x.UpdateDriver(108, "bb", 55, time, 40, 40, 1), Times.Once);
        }

        /// <summary>
        /// Checks that driver is not CREATEd when inputting hiredate that is in the future.
        /// </summary>
        [Test]
        public void WhenCreatingDriver_AndInputDateIsInTheFuture_DriverIsNotCreated()
        {
            DateTime time = DateTime.Now.AddYears(1);
            this.driverLogic.CreateDriver(2222, "bb", 25, time, 40, 40, 1);
            Assert.That(this.driverLogic.GetOneDriver(2222), Is.Null);
            this.mockDrivers.Verify(x => x.CreateDriver(2222, "bb", 25, time, 40, 40, 1), Times.Never);
        }

        /// <summary>
        /// When hireddate is in the past, driver is CREATEd.
        /// </summary>
        [Test]
        public void WhenCreatingDriver_AndInputDateIsInThePast_DriverIsCreated()
        {
            DateTime time = DateTime.Now.AddYears(-2);
            this.driverLogic.CreateDriver(3333, "bb", 25, time, 40, 40, 1);
            Assert.That(this.driverLogic.GetOneDriver(3333), Is.Not.Null);
            this.mockDrivers.Verify(x => x.CreateDriver(3333, "bb", 25, time, 40, 40, 1), Times.Once);
        }

        /// <summary>
        /// Checks that driver is not UPDATEd when inputting hiredate that is in the future.
        /// </summary>
        [Test]
        public void WhenUpdatingDriver_AndInputDateIsInTheFuture_DriverIsNotModified()
        {
            DateTime time = DateTime.Now.AddYears(1);
            this.driverLogic.UpdateDriver(100, "HiredInTheFuture", 25, time, 40, 40, 1);
            Assert.That(this.driverLogic.GetOneDriver(100).driver_name, Is.Not.EqualTo("HiredInTheFuture"));
            this.mockDrivers.Verify(x => x.UpdateDriver(100, "HiredInTheFuture", 25, time, 40, 40, 1), Times.Never);
        }

        /// <summary>
        /// Testing GETALL.
        /// </summary>
        [Test]
        public void WhenMoreThanOneDriversExist_AndQueryingAllDrivers_NotEmptyIsReturned()
        {
            List<drivers> myDrivers = this.driverLogic.GetAll().ToList();
            Assert.That(myDrivers, Is.Not.Empty);
        }

        /// <summary>
        /// (6) Get all the coordinators & dispatchers stationed at Újpest-Központ.
        /// </summary>
        [Test]
        public void WhenACoordinatorAtUjpestKozpontExists_AndTryingToFetchThatCoordinator_ThatCoordinatorIsReturned()
        {
            NonCrudOperationLogic nonCrud1 = new NonCrudOperationLogic();
            List<coordsAndDispatchers> coordAtUjpestKozpont = nonCrud1.GetCoordinatorsAtStation(this.coordLogic, this.stationLogic, "Újpest-Központ");

            Assert.That(coordAtUjpestKozpont.FirstOrDefault().coord_stationId, Is.EqualTo(1)); // 1 = ÚJPEST-KÖZPONT
        }

        /// <summary>
        /// Searching for a station that has no coordinators.
        /// </summary>
        [Test]
        public void WhenCoordinatorAtKobanyaKispestDoesntExist_AndTryingToFetchSuchCoordinator_ReturnsNull()
        {
            NonCrudOperationLogic nonCrud1 = new NonCrudOperationLogic();
            List<coordsAndDispatchers> coordAtUjpestKozpont = nonCrud1.GetCoordinatorsAtStation(this.coordLogic, this.stationLogic, "Kőbánya-Kispest");

            Assert.That(coordAtUjpestKozpont.FirstOrDefault, Is.Null);
        }

        /// <summary>
        /// (7) Get driver whose last used vehicle had at least 5000 hours driven.
        /// </summary>
        [Test]
        public void WhenDriverWithLastUsedVehicleWith5000HrsDrivenExists_AndTryingToFetchThatDriver_ItIsReturned()
        {
            NonCrudOperationLogic noncrud1 = new NonCrudOperationLogic();
            List<drivers> drivers = noncrud1.GetDriverWithVehicleDrivenForSpecificHours(this.driverLogic, this.vehicleLogic, 5000);
            Assert.That(drivers.FirstOrDefault().driver_lastDrivenVehicleId, Is.EqualTo(1));
        }

        /// <summary>
        /// Returning null, since there is not vehicle with 9999 hours.
        /// </summary>
        [Test]
        public void WhenDriverWithLastUsedVehicleWith9999HrsDrivenDoesNotExist_AndTryingToFetchThatDriver_ReturnsNullDriver()
        {
            NonCrudOperationLogic noncrud1 = new NonCrudOperationLogic();
            List<drivers> drivers = noncrud1.GetDriverWithVehicleDrivenForSpecificHours(this.driverLogic, this.vehicleLogic, 9999);
            Assert.That(drivers.FirstOrDefault(), Is.Null);
        }

        /// <summary>
        /// (8) Get ticket inspector whose last inspected vehicle was last repaired more than 1 year ago.
        /// </summary>
        [Test]
        public void WhenTicketInspectorWithLastUsedVehicleThatHasLastRepairedDateOfMoreThanOneYear_AndTryingToFetchThatTicketInspector_ItIsReturned()
        {
            NonCrudOperationLogic noncrud1 = new NonCrudOperationLogic();
            List<inspectors> inspectors = noncrud1.GetInspectorWhoseLastVehicleWasRepairedMoreThanSpecificTimeAgo(this.inspectorLogic, this.vehicleLogic, DateTime.Now.AddYears(-1));
            Assert.That(inspectors.FirstOrDefault().inspector_lastInspectedVehicleId, Is.EqualTo(1));
        }

        /// <summary>
        /// Does not return a ticket inspector for a vehicle repairs date that is in the medieval age.
        /// </summary>
        [Test]
        public void WhenVehicleLastRepaired500YearsAgoDoesNotExist_AndTryingToFetchAnInspectorWithSuchALastUsedVehicle_ReturnsNull()
        {
            NonCrudOperationLogic noncrud1 = new NonCrudOperationLogic();
            List<inspectors> inspectors = new List<inspectors>();
            inspectors = noncrud1.GetInspectorWhoseLastVehicleWasRepairedMoreThanSpecificTimeAgo(this.inspectorLogic, this.vehicleLogic, DateTime.Now.AddYears(-500));
            Assert.That(inspectors, Is.Empty);
        }
    }
}
