﻿namespace BKV.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;

    public interface IDriverRepository : IRepository<drivers>
    {
        // void ChangeName(int id, int age);
        void CreateDriver(int id, string name, int age, DateTime hiredDate, int avgHoursPerWeek, int hoursWorkedTotal, int lastDrivenVehicleID);

        drivers ReadDriver(int id);

        void UpdateDriver(int id, string name, int age, DateTime hiredDate, int avgHoursPerWeek, int hoursWorkedTotal, int lastDrivenVehicleID);

        void DeleteDriver(int id);

    }
}
