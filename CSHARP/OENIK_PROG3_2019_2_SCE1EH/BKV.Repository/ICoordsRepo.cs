﻿namespace BKV.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;

    public interface ICoordsRepo : IRepository<coordsAndDispatchers>
    {
        void CreateCoord(int id, string name, int age, DateTime hiredDate, int avgHrsPerWeek, int hrsWorkedTotal, int stationId);

        coordsAndDispatchers ReadCoord(int id);

        void UpdateCoord(int id, string name, int age, DateTime hiredDate, int avgHrsPerWeek, int hrsWorkedTotal, int stationId);

        void DeleteCoord(int id);
    }
}
