﻿namespace BKV.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;

    /// <summary>
    /// Repo for drivers.
    /// </summary>
    public class DriverRepository : Repository<drivers>, IDriverRepository
    {
        public DriverRepository(BKVDatabaseEntities ctx)
            : base(ctx)
        {
        }

        public void CreateDriver(int id, string name, int age, DateTime hiredDate, int avgHoursPerWeek, int hoursWorkedTotal, int lastDrivenVehicleID)
        {
            drivers driver = new drivers();

            driver.driver_id = id;
            driver.driver_age = age;
            driver.driver_name = name;
            driver.driver_hiredDate = hiredDate;
            driver.driver_avgHoursPerWeek = avgHoursPerWeek;
            driver.driver_hoursWorkedTotal = hoursWorkedTotal;
            driver.driver_lastDrivenVehicleId = lastDrivenVehicleID;

            this.ctx.drivers.Add(driver);

            this.ctx.SaveChanges();
        }

        public drivers ReadDriver(int id)
        {
            drivers driver = new drivers();

            driver = this.ctx.drivers.Single(x => x.driver_id == id);

            return driver;
        }

        public void UpdateDriver(int id, string name, int age, DateTime hiredDate, int avgHoursPerWeek, int hoursWorkedTotal, int lastDrivenVehicleID)
        {
            drivers driver = new drivers();

            driver = ctx.drivers.Single(x => x.driver_id == id);

            driver.driver_age = age;
            driver.driver_name = name;
            driver.driver_hiredDate = hiredDate;
            driver.driver_avgHoursPerWeek = avgHoursPerWeek;
            driver.driver_hoursWorkedTotal = hoursWorkedTotal;
            driver.driver_lastDrivenVehicleId = lastDrivenVehicleID;

            ctx.SaveChanges();
        }

        public void DeleteDriver(int id)
        {
            drivers driver = new drivers();

            driver = this.ctx.drivers.Single(x => x.driver_id == id);

            this.ctx.drivers.Remove(driver);

            this.ctx.SaveChanges();
        }

        public override drivers GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.driver_id == id);
        }

        /*public void ChangeName(int id, int age)
        {
            var driver = GetOne(id);
            driver.driver_age = age;
            ctx.SaveChanges();
        }*/
    }
}
