﻿namespace BKV.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;

    /// <summary>
    /// Repo for inspectors.
    /// </summary>
    public class InspectorRepository : Repository<inspectors>, IInspectorRepository
    {
        public InspectorRepository(BKVDatabaseEntities ctx)
            : base(ctx) { }

        public void CreateInspector(int id, string name, int age, DateTime hiredDate, int avgHrsPerWeek, int hrsWorkedTotal, int lastInspectedVehicleId)
        {
            inspectors inspector = new inspectors();
            inspector.inspector_id = id;
            inspector.inspector_name = name;
            inspector.inspector_age = age;
            inspector.inspector_hiredDate = hiredDate;
            inspector.inspector_avgHoursPerWeek = avgHrsPerWeek;
            inspector.inspector_hoursWorkedTotal = hrsWorkedTotal;
            inspector.inspector_lastInspectedVehicleId = lastInspectedVehicleId;

            this.ctx.inspectors.Add(inspector);

            this.ctx.SaveChanges();
        }

        public void DeleteInspector(int id)
        {
            inspectors inspector = new inspectors();

            inspector = this.ctx.inspectors.Single(x => x.inspector_id == id);

            this.ctx.inspectors.Remove(inspector);

            this.ctx.SaveChanges();
        }

        public override inspectors GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.inspector_id == id);
        }


        public inspectors ReadInspector(int id)
        {
            inspectors inspector = new inspectors();

            inspector = this.ctx.inspectors.Single(x => x.inspector_id == id);

            return inspector;
        }

        public void UpdateInspector(int id, string name, int age, DateTime hiredDate, int avgHrsPerWeek, int hrsWorkedTotal, int lastInspectedVehicleId)
        {
            inspectors inspector = new inspectors();

            inspector.inspector_id = id;
            inspector.inspector_name = name;
            inspector.inspector_age = age;
            inspector.inspector_hiredDate = hiredDate;
            inspector.inspector_avgHoursPerWeek = avgHrsPerWeek;
            inspector.inspector_hoursWorkedTotal = hrsWorkedTotal;
            inspector.inspector_lastInspectedVehicleId = lastInspectedVehicleId;

            ctx.SaveChanges();
        }
    }
}
