﻿namespace BKV.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;

    /// <summary>
    /// Repo for the stations.
    /// </summary>
    public class StationRepository : Repository<stations>, IStationRepository
    {
        public StationRepository(BKVDatabaseEntities ctx)
            : base(ctx)
        {
        }

        public void CreateStation(int id, string name, int personnelAmount, bool cassaAvailability, bool ticketMachineAvailability, DateTime lastRepairsDate)
        {
            stations station = new stations();

            station.station_id = id;
            station.station_name = name;
            station.station_personnelAmount = personnelAmount;

            if (ticketMachineAvailability == true)
            {
                station.station_ticketMachineAvailability = 1;
            }
            else
            {
                station.station_ticketMachineAvailability = 0;
            }

            if (cassaAvailability == true)
            {
                station.station_cassaAvailability = 1;
            }
            else
            {
                station.station_cassaAvailability = 0;
            }

            station.station_lastRepairsDate = lastRepairsDate;

            this.ctx.stations.Add(station);
            ctx.SaveChanges();
        }

        public void DeleteStation(int id)
        {
            stations station = new stations();
            station = ctx.stations.Single(x => x.station_id == id);
            ctx.stations.Remove(station);
            ctx.SaveChanges();
        }

        public override stations GetOne(int id)
        {
            return GetAll().SingleOrDefault(x => x.station_id == id);
        }

        public stations ReadStation(int id)
        {
            stations station = new stations();
            station = ctx.stations.Single(x => x.station_id == id);
            return station;
        }

        public void UpdateStation(int id, string name, int personnelAmount, bool cassaAvailability, bool ticketMachineAvailability, DateTime lastRepairsDate)
        {
            stations station = new stations();

            station.station_name = name;
            station.station_personnelAmount = personnelAmount;

            if (ticketMachineAvailability == true)
            {
                station.station_ticketMachineAvailability = 1;
            }
            else
            {
                station.station_ticketMachineAvailability = 0;
            }

            if (cassaAvailability == true)
            {
                station.station_cassaAvailability = 1;
            }
            else
            {
                station.station_cassaAvailability = 0;
            }

            station.station_lastRepairsDate = lastRepairsDate;

            ctx.SaveChanges();
        }
    }
}
