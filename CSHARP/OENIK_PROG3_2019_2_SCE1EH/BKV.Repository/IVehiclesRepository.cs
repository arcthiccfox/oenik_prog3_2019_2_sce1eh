﻿namespace BKV.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;

    public interface IVehiclesRepository : IRepository<vehicles>
    {
        void CreateVehicle(int id, int typeId, string serialNum, string lineNum, DateTime dateManufactured, DateTime lastInspectedDate, int totalHrsDroven, DateTime lastRepairsDate);

        vehicles ReadVehicle(int id);

        void UpdateVehicle(int id, int typeId, string serialNum, string lineNum, DateTime dateManufactured, DateTime lastInspectedDate, int totalHrsDroven, DateTime lastRepairsDate);

        void DeleteVehicle(int id);
    }
}
