﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BKV.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;

    /// <summary>
    /// Generic repository class.
    /// </summary>
    public abstract class Repository<T> : IRepository<T> where T : class
    {
        protected BKVDatabaseEntities ctx;

        public Repository(BKVDatabaseEntities ctx)
        {
            this.ctx = ctx;
        }

        public IQueryable<T> GetAll()
        {
            return ctx.Set<T>();
        }

        public abstract T GetOne(int id);
    }
}
