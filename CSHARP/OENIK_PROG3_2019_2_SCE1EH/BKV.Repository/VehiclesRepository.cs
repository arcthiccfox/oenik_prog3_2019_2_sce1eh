﻿namespace BKV.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;

    /// <summary>
    /// Repo for the individual vehicles.
    /// </summary>
    public class VehiclesRepository : Repository<vehicles>, IVehiclesRepository
    {
        public VehiclesRepository(BKVDatabaseEntities ctx)
            : base(ctx)
        {
        }

        public void CreateVehicle(int id, int typeId, string serialNum, string lineNum, DateTime dateManufactured, DateTime lastInspectedDate, int totalHrsDroven, DateTime lastRepairsDate)
        {
            vehicles vehicle = new vehicles();

            vehicle.vehicle_id = id;
            vehicle.vehicle_typeId = typeId;
            vehicle.vehicle_serialnum = serialNum;
            vehicle.vehicle_dateManufactured = dateManufactured;
            vehicle.vehicle_linenumber = lineNum;
            vehicle.vehicle_dateManufactured = dateManufactured;
            vehicle.vehicle_lastInspectedDate = lastInspectedDate;
            vehicle.vehicle_totalHoursDriven = totalHrsDroven;
            vehicle.vehicle_lastRepairsDate = lastRepairsDate;

            this.ctx.vehicles.Add(vehicle);

            ctx.SaveChanges();
        }

        public void DeleteVehicle(int id)
        {
            vehicles vehicle = new vehicles();

            vehicle = ctx.vehicles.Single(x => x.vehicle_id == id);

            ctx.vehicles.Remove(vehicle);

            ctx.SaveChanges();
        }

        public override vehicles GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.vehicle_id == id);
        }

        public vehicles ReadVehicle(int id)
        {
            vehicles vehicle = new vehicles();

            vehicle = this.ctx.vehicles.Single(x => x.vehicle_id == id);

            return vehicle;
        }

        public void UpdateVehicle(int id, int typeId, string serialNum, string lineNum, DateTime dateManufactured, DateTime lastInspectedDate, int totalHrsDroven, DateTime lastRepairsDate)
        {
            vehicles vehicle = new vehicles();
            vehicle = ctx.vehicles.Single(x => x.vehicle_id == id);

            vehicle.vehicle_typeId = typeId;
            vehicle.vehicle_serialnum = serialNum;
            vehicle.vehicle_dateManufactured = dateManufactured;
            vehicle.vehicle_linenumber = lineNum;
            vehicle.vehicle_dateManufactured = dateManufactured;
            vehicle.vehicle_lastInspectedDate = lastInspectedDate;
            vehicle.vehicle_totalHoursDriven = totalHrsDroven;
            vehicle.vehicle_lastRepairsDate = lastRepairsDate;

            ctx.SaveChanges();
        }
    }
}
