﻿-- Kőhegyi Marcell SCE1EH

----- BKV DROP TABLES
 IF object_id('inspectors','U') is not null DROP TABLE inspectors;
 IF object_id('drivers','U') is not null DROP TABLE drivers;
 IF object_id('vehicles','U') is not null DROP TABLE vehicles;
 IF object_id('vTypes','U') is not null DROP TABLE vTypes;
 IF object_id('coordsAndDispatchers','U') is not null DROP TABLE coordsAndDispatchers;
 IF object_id('stations','U') is not null DROP TABLE stations;
----- BKV CREATE TABLES
GO
CREATE TABLE stations(
	station_id int primary key,
	station_name nvarchar(200),
	station_personnelAmount int,
	station_cassaAvailability tinyint, -- 0: no , 1: yes
	station_ticketMachineAvailability tinyint, -- 0: no, 1: yes
	station_lastRepairsDate date
);
CREATE TABLE coordsAndDispatchers(
	coord_id int primary key,
	coord_name nvarchar(200),
	coord_age int,
	coord_hiredDate date,
	coord_avgHoursPerWeek int,
	coord_hoursWorkedTotal int,
	coord_stationId int references stations(station_id)
);
CREATE TABLE vTypes( --  vehicletypes
	vType_id int primary key,
	vType_typename nvarchar(100),
	vType_modelname nvarchar(200),
	vType_maxSpeed int,
	vType_maxPassengers int,
	vType_fueltype nvarchar(100),
);
CREATE TABLE vehicles(
	vehicle_id int primary key,
	vehicle_typeId int references vTypes(vType_id),
	vehicle_serialnum nvarchar(100),
	vehicle_linenumber nvarchar(50),
	vehicle_dateManufactured date,
	vehicle_lastInspectedDate date,
	vehicle_totalHoursDriven int,
	vehicle_lastRepairsDate date
);
CREATE TABLE inspectors(
	inspector_id int primary key,
	inspector_name nvarchar(200),
	inspector_age int,
	inspector_hiredDate date, --time is not needed, only the day the person is hired on
	inspector_avgHoursPerWeek int,
	inspector_hoursWorkedTotal int,
	inspector_lastInspectedVehicleId int references vehicles(vehicle_id)
);

CREATE TABLE drivers(
	driver_id int primary key,
	driver_name nvarchar(200),
	driver_age int,
	driver_hiredDate date,
	driver_avgHoursPerWeek int,
	driver_hoursWorkedTotal int,
	driver_lastDrivenVehicleId int references vehicles(vehicle_id)
);
GO
----- BKV INSERT DATA

--					  id, typename, modelname, maxspeed, maxpassanger, fueltype
INSERT INTO vTypes VALUES (1, 'bus', 'IKARUS 260', 80, 98, 'diesel');
INSERT INTO vTypes VALUES (2, 'bus', 'IKARUS 435', 80, 157, 'diesel');
INSERT INTO vTypes VALUES (3, 'bus', 'VOLVO 7000A', 130, 104, 'diesel' );
INSERT INTO vTypes VALUES (4, 'bus', 'Mercedes-Benz Citaro C2 ', 140, 106, 'diesel');
INSERT INTO vTypes VALUES (5, 'tram', 'Ganz CSMG', 60, 203, 'electricity');
INSERT INTO vTypes VALUES (6, 'tram', 'ČKD Tatra T5C5 ', 60, 101, 'electricity');
INSERT INTO vTypes VALUES (7, 'tram', 'CAF Urbos', 80, 222, 'electricity');
INSERT INTO vTypes VALUES (8, 'trollybus', 'Ikarus 280T', 90, 143, 'electricity');
INSERT INTO vTypes VALUES (9, 'metro', 'Metrovagonmas 81–717/714', 90, 320, 'electricity');
INSERT INTO vTypes VALUES (10, 'metro', 'Alstom Metropolis', 70, 600, 'electricity'); 
INSERT INTO vTypes VALUES (11, 'metro', 'Ganz MFAV', 50, 80, 'electricity');
INSERT INTO vTypes VALUES (12, 'HÉV', 'LEW MXA', 90, 170, 'electricity');
INSERT INTO vTypes VALUES (13, 'service vehicle', 'Skoda Roomster', 210, 5, 'benzin');

--						id,		station_name, personnel, cassa, ticketmachine, lastrepairs
INSERT INTO stations VALUES (1, 'Újpest-Központ', 35, 0, 1, '2017-11-05');
INSERT INTO stations VALUES (2, 'Szentlélek tér', 50, 1, 1, '2018-06-01');
INSERT INTO stations VALUES (3, 'Nyugati-pályaudvar', 120, 1,1,'2001-05-05');
INSERT INTO stations VALUES (4, 'Keleti-pályaudvar', 144, 1,1, null);
INSERT INTO stations VALUES (5, 'Margit híd budai hídfő', 50, 0, 1, '2013-07-01');
INSERT INTO stations VALUES (6, 'Kelenföld vasútállomás', 80, 1, 1, '2017-10-11');
INSERT INTO stations VALUES (7, 'Déli pályaudvar', 120, 1, 1, '2018-10-20');
INSERT INTO stations VALUES (8, 'Széll Kálmán tér', 75, 0, 1, '2017-04-01');
INSERT INTO stations VALUES (9, 'Blaha Lujza tér', 40, 1,1, '2015-09-10');
INSERT INTO stations VALUES (10, 'Kálvin tér', 50, 1, 1, '2017-07-11');

--there is exactly one vehicle from every type
--					-id, typeId, serialnum, linenum, date manufactured, last inspected, hours driven, last repairs
INSERT INTO vehicles VALUES (1, 1, 'BPO-321', '220', '1997-05-05', '2018-10-10', 9000, '2014-04-01');
INSERT INTO vehicles VALUES (2, 2, 'BPO-221', '20E', '1995-11-10', '2018-09-15', 11000, '2016-06-22');
INSERT INTO vehicles VALUES (3, 3, 'BPO-532', '5', '2015-05-01', '2018-10-05', 3500, '2017-01-25');
INSERT INTO vehicles VALUES (4, 4, 'BPO-754', '9', '2017-08-11', '2018-08-11', 1200, null);
INSERT INTO vehicles VALUES (5, 5, '5213', '17', '1983-03-22', '2018-08-29', 14000, '2018-02-03' );
INSERT INTO vehicles VALUES (6, 6, '2321', '14', '1997-06-06', '2018-09-26', 8500, '2017-11-22' );
INSERT INTO vehicles VALUES (7, 7, '7632', '1', '2016-04-02', '2018-08-02', 2500, '2018-04-11' );
INSERT INTO vehicles VALUES (8, 8, 'BPO-221', '76', '1989-02-22', '2018-05-01', 12300, '2018-04-27' );
INSERT INTO vehicles VALUES (9, 9, '2673', 'M3', '1983-11-11', '2018-04-11', 15000, '2017-05-11');
INSERT INTO vehicles VALUES (10, 10, '2020', 'M2', '2011-02-07', '2018-08-11', 5500, '2017-10-12' );
INSERT INTO vehicles VALUES (11, 11, '5225', 'M1', '1978-01-01', '2018-10-11', 16700, '2018-04-05');
INSERT INTO vehicles VALUES (12, 12, '2452', 'H5', '1999-09-12', '2018-10-24', 7800, '2017-05-15');
INSERT into vehicles VALUES (13, 13, 'LZX-395', null, '2011-12-21', null, 5000, '2016-12-11');

--  exactly one person for each station
--									id, name, age, date hired, avg hours per week, total hours, stationId 
INSERT INTO coordsAndDispatchers VALUES (1, 'Kiss József', 35, '2008-01-01', 40, 6000, 1);
INSERT INTO coordsAndDispatchers VALUES (2, 'Várady Sándor', 25, '2011-03-21', 42, 5000, 2);
INSERT INTO coordsAndDispatchers VALUES (3, 'Tatai Lujza', 44, '2002-06-11', 30, 9000, 3);
INSERT INTO coordsAndDispatchers VALUES (4, 'Kassai András', 27, '2015-03-30', 38, 3000, 4);
INSERT INTO coordsAndDispatchers VALUES (5, 'Almássy József', 41, '2004-11-06', 36, 14000, 5);
INSERT INTO coordsAndDispatchers VALUES (6, 'Kovács Titusz', 34, '2012-05-16', 39, 8000, 6);
INSERT INTO coordsAndDispatchers VALUES (7, 'Erdei Ádám', 25, '2013-05-26', 41, 4000, 7);
INSERT INTO coordsAndDispatchers VALUES (8, 'Hosszú Andrea', 27, '2018-02-22', 32, 1000, 8);
INSERT INTO coordsAndDispatchers VALUES (9, 'Szegedi Laura', 36, '2009-12-11', 42, 9000, 9);
INSERT INTO coordsAndDispatchers VALUES (10, 'Somlói Sarolta', 49, '2002-04-02', 37, 12000, 10);

-- exactly as many inspectors and drivers as vehicles, excluding the service vehicle
--							id, name, age, date hired, avg hours per week, total hours, last vehicleid
INSERT INTO inspectors VALUES (1, 'Nagy Béla', 40, '2004-01-01', 40, 12000, 1);
INSERT INTO inspectors VALUES (2, 'Váci Endre', 31, '2010-05-12', 43, 6000, 2);
INSERT INTO inspectors VALUES (3, 'Hadváry Péter', 38, '2008-02-21', 38, 9000, 3);
INSERT INTO inspectors VALUES (4, 'Sikesi István', 34, '2014-10-02', 35, 5000, 4);
INSERT INTO inspectors VALUES (5, 'Holló András', 34, '2016-04-19', 44, 4000, 5);
INSERT INTO inspectors VALUES (6, 'Kőröss Csanád', 24, '2017-11-21', 40, 12000, 6);
INSERT INTO inspectors VALUES (7, 'Alma Sándor', 40, '2014-03-14', 42, 7000, 7);
INSERT INTO inspectors VALUES (8, 'Huszár Béla', 40, '2012-07-16', 41, 8000, 8);
INSERT INTO inspectors VALUES (9, 'Sende György', 40, '2009-07-11', 36, 11000, 9);
INSERT INTO inspectors VALUES (10, 'Hegedűs Péter', 40, '2002-03-04', 39, 13000, 10);
INSERT INTO inspectors VALUES (11, 'Sánci Sándor', 40, '2005-05-05', 35, 12000, 11);
INSERT INTO inspectors VALUES (12, 'Helmessi György', 40, '2007-03-07', 41, 11000, 12);


INSERT INTO drivers VALUES (1, 'Hegedűs János', 34, '2017-03-01', 40, 2000, 1);
INSERT INTO drivers VALUES (2, 'Kerekes Ica', 24, '2017-03-01', 33, 3500, 2);
INSERT INTO drivers VALUES (3, 'Szabó Laura', 25, '2017-03-01', 34, 3000, 3);
INSERT INTO drivers VALUES (4, 'Malmos Krisztián', 45, '2017-03-01', 37, 4000, 4);
INSERT INTO drivers VALUES (5, 'Pék Ádám', 41, '2017-03-01', 42, 3000, 5);
INSERT INTO drivers VALUES (6, 'Lovass Béla', 42, '2004-04-19', 45, 9000, 6);
INSERT INTO drivers VALUES (7, 'Hegyi Károly', 36, '2003-05-03', 41, 12000, 7);
INSERT INTO drivers VALUES (8, 'Hűvőssi István', 36, '2007-07-05', 38, 8000, 8);
INSERT INTO drivers VALUES (9, 'Újváry Péter', 33, '2014-05-19', 39, 3000, 9);
INSERT INTO drivers VALUES (10, 'Ürömi Sándor', 34, '2013-01-17', 41, 4000, 10);
INSERT INTO drivers VALUES (11, 'Völgyi Károly', 34, '2013-08-13', 36, 4000, 11);
INSERT INTO drivers VALUES (12, 'Borsodi Lóránt', 34, '2012-06-21', 35, 5000, 12);
GO
select * from vTypes;
select * from vehicles;
select * from stations;
select * from coordsAndDispatchers;
select * from inspectors;
select * from drivers;
