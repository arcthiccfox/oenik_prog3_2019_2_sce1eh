﻿namespace BKV.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;

    public interface IStationRepository : IRepository<stations>
    {
        void CreateStation(int id, string name, int personnelAmount, bool cassaAvailability, bool ticketMachineAvailability, DateTime lastRepairsDate);

        stations ReadStation(int id);

        void UpdateStation(int id, string name, int personnelAmount, bool cassaAvailability, bool ticketMachineAvailability, DateTime lastRepairsDate);

        void DeleteStation(int id);
    }
}
