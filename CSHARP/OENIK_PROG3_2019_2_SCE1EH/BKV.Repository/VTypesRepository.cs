﻿namespace BKV.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;

    /// <summary>
    /// Repo for the vehicle types.
    /// </summary>
    public class VTypesRepository : Repository<vTypes>, IVTypesRepository
    {
        public VTypesRepository(BKVDatabaseEntities ctx)
            : base(ctx)
        {
        }

        public void CreateVType(int id, string typeName, string modelName, int maxSpeed, int maxPassangers, string fuelType)
        {
            vTypes vType = new vTypes();

            vType.vType_id = id;
            vType.vType_typename = typeName;
            vType.vType_modelname = modelName;
            vType.vType_maxSpeed = maxSpeed;
            vType.vType_maxPassengers = maxPassangers;
            vType.vType_fueltype = fuelType;

            this.ctx.vTypes.Add(vType);

            ctx.SaveChanges();
        }

        public void DeleteVType(int id)
        {
            vTypes vType = new vTypes();

            vType = ctx.vTypes.Single(x => x.vType_id == id);

            ctx.vTypes.Remove(vType);

            ctx.SaveChanges();
        }

        public override vTypes GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.vType_id == id);
        }

        public vTypes ReadVType(int id)
        {
            vTypes vType = new vTypes();

            vType = this.ctx.vTypes.Single(x => x.vType_id == id);

            return vType;
        }

        public void UpdateVType(int id, string typeName, string modelName, int maxSpeed, int maxPassangers, string fuelType)
        {
            vTypes vType = new vTypes();
            vType = ctx.vTypes.Single(x => x.vType_id == id);

            vType.vType_typename = typeName;
            vType.vType_modelname = modelName;
            vType.vType_maxSpeed = maxSpeed;
            vType.vType_maxPassengers = maxPassangers;
            vType.vType_fueltype = fuelType;

            ctx.SaveChanges();
        }
    }
}
