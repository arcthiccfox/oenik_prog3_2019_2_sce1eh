﻿namespace BKV.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;

    /// <summary>
    /// Repo of coordinators.
    /// </summary>
    public class CoordsRepo : Repository<coordsAndDispatchers>, ICoordsRepo
    {
        public CoordsRepo(BKVDatabaseEntities ctx)
            : base(ctx)
        {
        }

        public void CreateCoord(int id, string name, int age, DateTime hiredDate, int avgHrsPerWeek, int hrsWorkedTotal, int stationId)
        {
            coordsAndDispatchers coord = new coordsAndDispatchers();

            coord.coord_id = id;
            coord.coord_name = name;
            coord.coord_age = age;
            coord.coord_hiredDate = hiredDate;
            coord.coord_avgHoursPerWeek = avgHrsPerWeek;
            coord.coord_hoursWorkedTotal = hrsWorkedTotal;
            coord.coord_stationId = stationId;

            this.ctx.coordsAndDispatchers.Add(coord);

            this.ctx.SaveChanges();
        }

        public void DeleteCoord(int id)
        {
            coordsAndDispatchers coord = new coordsAndDispatchers();
            coord = this.ctx.coordsAndDispatchers.Single(x => x.coord_id == id);
            this.ctx.coordsAndDispatchers.Remove(coord);
            this.ctx.SaveChanges();
        }

        public override coordsAndDispatchers GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.coord_id == id);
        }

        public coordsAndDispatchers ReadCoord(int id)
        {
            coordsAndDispatchers coord = new coordsAndDispatchers();
            coord = ctx.coordsAndDispatchers.Single(x => x.coord_id == id);
            return coord;
        }

        public void UpdateCoord(int id, string name, int age, DateTime hiredDate, int avgHrsPerWeek, int hrsWorkedTotal, int stationId)
        {
            coordsAndDispatchers coord = new coordsAndDispatchers();

            coord.coord_name = name;
            coord.coord_age = age;
            coord.coord_hiredDate = hiredDate;
            coord.coord_avgHoursPerWeek = avgHrsPerWeek;
            coord.coord_hoursWorkedTotal = hrsWorkedTotal;
            coord.coord_stationId = stationId;

            ctx.coordsAndDispatchers.Add(coord);

            ctx.SaveChanges();
        }
    }
}
