﻿namespace BKV.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;

    public interface IVTypesRepository : IRepository<vTypes>
    {
        void CreateVType(int id, string typeName, string modelName, int maxSpeed, int maxPassangers, string fuelType);

        vTypes ReadVType(int id);

        void UpdateVType(int id, string typeName, string modelName, int maxSpeed, int maxPassangers, string fuelType);

        void DeleteVType(int id);
    }
}
