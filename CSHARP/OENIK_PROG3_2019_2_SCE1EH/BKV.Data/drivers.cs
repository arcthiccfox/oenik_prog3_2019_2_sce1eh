//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BKV.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class drivers
    {
        public int driver_id { get; set; }
        public string driver_name { get; set; }
        public Nullable<int> driver_age { get; set; }
        public Nullable<System.DateTime> driver_hiredDate { get; set; }
        public Nullable<int> driver_avgHoursPerWeek { get; set; }
        public Nullable<int> driver_hoursWorkedTotal { get; set; }
        public Nullable<int> driver_lastDrivenVehicleId { get; set; }
    
        public virtual vehicles vehicles { get; set; }
    }
}
