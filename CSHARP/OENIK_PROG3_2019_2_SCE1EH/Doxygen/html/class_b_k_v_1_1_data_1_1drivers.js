var class_b_k_v_1_1_data_1_1drivers =
[
    [ "driver_age", "class_b_k_v_1_1_data_1_1drivers.html#aade6fb1e51d5a4bc8f5dff7650f8fe8d", null ],
    [ "driver_avgHoursPerWeek", "class_b_k_v_1_1_data_1_1drivers.html#a24e4ec2e5707e0db19e94e1ddfccdb42", null ],
    [ "driver_hiredDate", "class_b_k_v_1_1_data_1_1drivers.html#ab3ad4f527926767de90947ef385ad629", null ],
    [ "driver_hoursWorkedTotal", "class_b_k_v_1_1_data_1_1drivers.html#abd29da0e291c18582807feed6c439e0a", null ],
    [ "driver_id", "class_b_k_v_1_1_data_1_1drivers.html#a3cb1988c6e1b6f1ac735636335896a40", null ],
    [ "driver_lastDrivenVehicleId", "class_b_k_v_1_1_data_1_1drivers.html#ad6162b872ca8a97d2374ec7a9cb4d9a9", null ],
    [ "driver_name", "class_b_k_v_1_1_data_1_1drivers.html#af5d90f9a80fb23c1f928950c01fedc84", null ],
    [ "vehicles", "class_b_k_v_1_1_data_1_1drivers.html#ac18fd477b5a7cdc0edc9f75820e14418", null ]
];