var interface_b_k_v_1_1_repository_1_1_i_station_repository =
[
    [ "CreateStation", "interface_b_k_v_1_1_repository_1_1_i_station_repository.html#a0589bc8457c8230c86c3333f0b4f8ba1", null ],
    [ "DeleteStation", "interface_b_k_v_1_1_repository_1_1_i_station_repository.html#a79d69c04a8da5080d539736e3faff571", null ],
    [ "ReadStation", "interface_b_k_v_1_1_repository_1_1_i_station_repository.html#a17e98db16fd405d3633efe058c88c89e", null ],
    [ "UpdateStation", "interface_b_k_v_1_1_repository_1_1_i_station_repository.html#a54d6723ca01fd373007df80a422e3da1", null ]
];