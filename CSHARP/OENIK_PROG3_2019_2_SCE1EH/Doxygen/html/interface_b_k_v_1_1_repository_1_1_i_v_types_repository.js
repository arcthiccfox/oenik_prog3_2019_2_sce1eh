var interface_b_k_v_1_1_repository_1_1_i_v_types_repository =
[
    [ "CreateVType", "interface_b_k_v_1_1_repository_1_1_i_v_types_repository.html#afc33ad6e71d9a1960e688e5d97f66ef9", null ],
    [ "DeleteVType", "interface_b_k_v_1_1_repository_1_1_i_v_types_repository.html#ac802bd5c66653b886a032bbb94b3e508", null ],
    [ "ReadVType", "interface_b_k_v_1_1_repository_1_1_i_v_types_repository.html#a7bf162218f6b5d2813118c7feeeb263f", null ],
    [ "UpdateVType", "interface_b_k_v_1_1_repository_1_1_i_v_types_repository.html#a74442949fab0c70fc11a82d26b3607d2", null ]
];