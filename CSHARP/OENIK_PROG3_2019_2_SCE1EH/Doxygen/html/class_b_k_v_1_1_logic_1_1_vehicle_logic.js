var class_b_k_v_1_1_logic_1_1_vehicle_logic =
[
    [ "VehicleLogic", "class_b_k_v_1_1_logic_1_1_vehicle_logic.html#a1eada9e66cbbab1fdf4bedbbc19f5be0", null ],
    [ "VehicleLogic", "class_b_k_v_1_1_logic_1_1_vehicle_logic.html#ab368a898b9fb97f8693e93c4f1d80039", null ],
    [ "CreateVehicle", "class_b_k_v_1_1_logic_1_1_vehicle_logic.html#a6baa49a224e4b0d971ff21d188bfc100", null ],
    [ "DeleteVehicle", "class_b_k_v_1_1_logic_1_1_vehicle_logic.html#a1b29d3d0bce3be21107900ac6a724d85", null ],
    [ "GetAll", "class_b_k_v_1_1_logic_1_1_vehicle_logic.html#a61f893e6fd2c1fb0c438d6cdf64de8aa", null ],
    [ "ReadVehicle", "class_b_k_v_1_1_logic_1_1_vehicle_logic.html#a260e18f14061bf71191c251e85e4b674", null ],
    [ "UpdateVehicle", "class_b_k_v_1_1_logic_1_1_vehicle_logic.html#a6442246d616c3cd5a63b9c7c896cb97e", null ]
];