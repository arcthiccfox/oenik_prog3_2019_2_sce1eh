var interface_b_k_v_1_1_logic_1_1_i_station_logic =
[
    [ "CreateStation", "interface_b_k_v_1_1_logic_1_1_i_station_logic.html#a0044b25e2b162d8c5913c6774b902ada", null ],
    [ "GetAll", "interface_b_k_v_1_1_logic_1_1_i_station_logic.html#a849cba7d2915287e0d95f0bb4ad309f9", null ],
    [ "ReadStation", "interface_b_k_v_1_1_logic_1_1_i_station_logic.html#a208105f7a85e82873d25f4db4e0920a5", null ],
    [ "RemoveStation", "interface_b_k_v_1_1_logic_1_1_i_station_logic.html#a7a8174a962ac9df3da2d5fd03e418bed", null ],
    [ "UpdateStation", "interface_b_k_v_1_1_logic_1_1_i_station_logic.html#aa0ff889ac3f5d30ef3572168e3e6969a", null ]
];