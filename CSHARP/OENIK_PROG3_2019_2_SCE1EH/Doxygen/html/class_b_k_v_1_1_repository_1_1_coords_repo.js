var class_b_k_v_1_1_repository_1_1_coords_repo =
[
    [ "CoordsRepo", "class_b_k_v_1_1_repository_1_1_coords_repo.html#a046a1390d0e867fbfdeedd6b39972c6f", null ],
    [ "CreateCoord", "class_b_k_v_1_1_repository_1_1_coords_repo.html#ad47665b354bde67f645480dd849a53b9", null ],
    [ "DeleteCoord", "class_b_k_v_1_1_repository_1_1_coords_repo.html#a2f90a6d5faead730d68a4da26aa04a1d", null ],
    [ "GetOne", "class_b_k_v_1_1_repository_1_1_coords_repo.html#a1d2e515298f84fae4f2e82dc758912be", null ],
    [ "ReadCoord", "class_b_k_v_1_1_repository_1_1_coords_repo.html#a51f5b2b46e45d7b236e1231fc9dd1644", null ],
    [ "UpdateCoord", "class_b_k_v_1_1_repository_1_1_coords_repo.html#ab4e21baf7464f58f9b4f6380d28684f7", null ]
];