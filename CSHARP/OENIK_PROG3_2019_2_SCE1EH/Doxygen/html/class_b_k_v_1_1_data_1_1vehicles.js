var class_b_k_v_1_1_data_1_1vehicles =
[
    [ "vehicles", "class_b_k_v_1_1_data_1_1vehicles.html#ac64e6e8fafb9ed22d0233af35a82c45a", null ],
    [ "drivers", "class_b_k_v_1_1_data_1_1vehicles.html#aaedc5cc6c8d13162656a04799a7828b9", null ],
    [ "inspectors", "class_b_k_v_1_1_data_1_1vehicles.html#a6d8ab2d02afa30b7aebaa7850cfedffc", null ],
    [ "vehicle_dateManufactured", "class_b_k_v_1_1_data_1_1vehicles.html#a786e956ce6239eea84d68c4bef131982", null ],
    [ "vehicle_id", "class_b_k_v_1_1_data_1_1vehicles.html#a2f12758e2744e24f753db024756785fe", null ],
    [ "vehicle_lastInspectedDate", "class_b_k_v_1_1_data_1_1vehicles.html#a5f65513bf004642c6f633b30739f4893", null ],
    [ "vehicle_lastRepairsDate", "class_b_k_v_1_1_data_1_1vehicles.html#a3356d83d6ecfdbe76e5d6843d629a749", null ],
    [ "vehicle_linenumber", "class_b_k_v_1_1_data_1_1vehicles.html#a1d1b5a00d2fa412091dd2910f9d2f8dd", null ],
    [ "vehicle_serialnum", "class_b_k_v_1_1_data_1_1vehicles.html#a486b402386f060a87ae80e60ce6f2c18", null ],
    [ "vehicle_totalHoursDriven", "class_b_k_v_1_1_data_1_1vehicles.html#a5b8e836b553507662e2bf5027e5bb672", null ],
    [ "vehicle_typeId", "class_b_k_v_1_1_data_1_1vehicles.html#a33bf020ae44d9a4bdda25ad8df97b787", null ],
    [ "vTypes", "class_b_k_v_1_1_data_1_1vehicles.html#a329dc2a6d3dd5ddc4c76add299f256f9", null ]
];