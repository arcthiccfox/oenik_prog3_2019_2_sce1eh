var interface_b_k_v_1_1_logic_1_1_i_coord_logic =
[
    [ "CreateCoord", "interface_b_k_v_1_1_logic_1_1_i_coord_logic.html#ae39f5a893a78b4504beb7046e1337eba", null ],
    [ "DeleteCoord", "interface_b_k_v_1_1_logic_1_1_i_coord_logic.html#a7aa1119ca92d8d52fb827218e4c34b58", null ],
    [ "GetAll", "interface_b_k_v_1_1_logic_1_1_i_coord_logic.html#a12768e1f67652384f1d6847063994a0f", null ],
    [ "ReadCoord", "interface_b_k_v_1_1_logic_1_1_i_coord_logic.html#ad942bd02ac2c8759b8f99ea634eda296", null ],
    [ "UpdateCoord", "interface_b_k_v_1_1_logic_1_1_i_coord_logic.html#acdeb229ebe702e75dd0cab0aae7b743f", null ]
];