var searchData=
[
  ['getall_15',['GetAll',['../class_b_k_v_1_1_logic_1_1_driver_logic.html#a0d20351129c3aa851d396d1f7af5a51e',1,'BKV::Logic::DriverLogic']]],
  ['getcoordinatorsatstation_16',['GetCoordinatorsAtStation',['../class_b_k_v_1_1_logic_1_1_non_crud_operation_logic.html#a0c26709c56b5fdc5c5632d36669820a0',1,'BKV::Logic::NonCrudOperationLogic']]],
  ['getdriverwithvehicledrivenforspecifichours_17',['GetDriverWithVehicleDrivenForSpecificHours',['../class_b_k_v_1_1_logic_1_1_non_crud_operation_logic.html#a1d22d4e6a5681f0e43ccc79f53806427',1,'BKV::Logic::NonCrudOperationLogic']]],
  ['getignumber_18',['GetiGNumber',['../class_b_k_v_1_1_program_1_1_program.html#a1b86d8a58cf985330bc8708821da65d7',1,'BKV::Program::Program']]],
  ['getinspectorwhoselastvehiclewasrepairedmorethanspecifictimeago_19',['GetInspectorWhoseLastVehicleWasRepairedMoreThanSpecificTimeAgo',['../class_b_k_v_1_1_logic_1_1_non_crud_operation_logic.html#ad3c8271b5584e936f9f4f6d0c633fd24',1,'BKV::Logic::NonCrudOperationLogic']]],
  ['getonedriver_20',['GetOneDriver',['../class_b_k_v_1_1_logic_1_1_driver_logic.html#aecae3e8ba4a728cfc3b180ff6614eb0b',1,'BKV::Logic::DriverLogic']]],
  ['getstreetnumber_21',['GetStreetNumber',['../class_b_k_v_1_1_program_1_1_program.html#aa8c9d0b7a526b1c35d303e2ca8b2f39f',1,'BKV::Program::Program']]]
];
