var class_b_k_v_1_1_logic_1_1_non_crud_operation_logic =
[
    [ "GetCoordinatorsAtStation", "class_b_k_v_1_1_logic_1_1_non_crud_operation_logic.html#a0c26709c56b5fdc5c5632d36669820a0", null ],
    [ "GetDriverWithVehicleDrivenForSpecificHours", "class_b_k_v_1_1_logic_1_1_non_crud_operation_logic.html#a1d22d4e6a5681f0e43ccc79f53806427", null ],
    [ "GetInspectorWhoseLastVehicleWasRepairedMoreThanSpecificTimeAgo", "class_b_k_v_1_1_logic_1_1_non_crud_operation_logic.html#ad3c8271b5584e936f9f4f6d0c633fd24", null ]
];