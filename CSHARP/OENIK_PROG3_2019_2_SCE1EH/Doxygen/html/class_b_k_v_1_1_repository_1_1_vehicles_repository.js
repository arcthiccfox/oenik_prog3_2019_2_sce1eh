var class_b_k_v_1_1_repository_1_1_vehicles_repository =
[
    [ "VehiclesRepository", "class_b_k_v_1_1_repository_1_1_vehicles_repository.html#adcce2c4fc232b89fb850c237c8f86901", null ],
    [ "CreateVehicle", "class_b_k_v_1_1_repository_1_1_vehicles_repository.html#a6f0407fa070287132efc30821486319c", null ],
    [ "DeleteVehicle", "class_b_k_v_1_1_repository_1_1_vehicles_repository.html#a002a7dd4112e87061a20024d27910872", null ],
    [ "GetOne", "class_b_k_v_1_1_repository_1_1_vehicles_repository.html#a398322f39651b71f5fad3144dfff72ce", null ],
    [ "ReadVehicle", "class_b_k_v_1_1_repository_1_1_vehicles_repository.html#aab35791391a7afb0b02f3e1a83e7d9a9", null ],
    [ "UpdateVehicle", "class_b_k_v_1_1_repository_1_1_vehicles_repository.html#a81b22ac05b123b8b025f3220dea7d35c", null ]
];