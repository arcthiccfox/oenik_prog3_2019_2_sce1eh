var class_b_k_v_1_1_data_1_1v_types =
[
    [ "vTypes", "class_b_k_v_1_1_data_1_1v_types.html#a2ab0932ebaf040f14bf519cbbc127cae", null ],
    [ "vehicles", "class_b_k_v_1_1_data_1_1v_types.html#ae29a1f9354a1d3feb4cdc8feb86ec429", null ],
    [ "vType_fueltype", "class_b_k_v_1_1_data_1_1v_types.html#a2b7fd3bc1e4b5fe1e32c39894bfc67d3", null ],
    [ "vType_id", "class_b_k_v_1_1_data_1_1v_types.html#a50ac5b86f8b259cdd8246cdf54412bf9", null ],
    [ "vType_maxPassengers", "class_b_k_v_1_1_data_1_1v_types.html#a6bcfdbf1619920709ad61e45755b19a4", null ],
    [ "vType_maxSpeed", "class_b_k_v_1_1_data_1_1v_types.html#a8313f49bca0db6026080508ea85b3d28", null ],
    [ "vType_modelname", "class_b_k_v_1_1_data_1_1v_types.html#ae016a26a90cb52ccc6ba54e3b6a5a4eb", null ],
    [ "vType_typename", "class_b_k_v_1_1_data_1_1v_types.html#abf95b6d4458fda179b9ebccffbbdec16", null ]
];