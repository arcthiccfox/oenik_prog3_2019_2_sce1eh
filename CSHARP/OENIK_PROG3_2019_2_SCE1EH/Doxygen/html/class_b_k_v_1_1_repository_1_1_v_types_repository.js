var class_b_k_v_1_1_repository_1_1_v_types_repository =
[
    [ "VTypesRepository", "class_b_k_v_1_1_repository_1_1_v_types_repository.html#a4c88953df9acb36e3aa71da29848eb81", null ],
    [ "CreateVType", "class_b_k_v_1_1_repository_1_1_v_types_repository.html#a53fa96773bf695a9d15baae41cfafe70", null ],
    [ "DeleteVType", "class_b_k_v_1_1_repository_1_1_v_types_repository.html#af9fda4cf2b829def9ac0a366570e24fb", null ],
    [ "GetOne", "class_b_k_v_1_1_repository_1_1_v_types_repository.html#afd86f94ca0aacb9a76a462dae7446d30", null ],
    [ "ReadVType", "class_b_k_v_1_1_repository_1_1_v_types_repository.html#a596c6c5ec5a83cf0277e627ffdbbd13d", null ],
    [ "UpdateVType", "class_b_k_v_1_1_repository_1_1_v_types_repository.html#a419dc90ec1a184467b7e9ae670d82410", null ]
];