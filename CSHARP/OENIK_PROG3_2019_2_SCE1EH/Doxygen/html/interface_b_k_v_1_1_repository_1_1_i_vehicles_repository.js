var interface_b_k_v_1_1_repository_1_1_i_vehicles_repository =
[
    [ "CreateVehicle", "interface_b_k_v_1_1_repository_1_1_i_vehicles_repository.html#a6f26e53a6f4217d3a0c59443e4024e81", null ],
    [ "DeleteVehicle", "interface_b_k_v_1_1_repository_1_1_i_vehicles_repository.html#a2a8bface30b5f14d78ec542e14703bf3", null ],
    [ "ReadVehicle", "interface_b_k_v_1_1_repository_1_1_i_vehicles_repository.html#ace80d6d561b8fbcf2946ad9fbc291011", null ],
    [ "UpdateVehicle", "interface_b_k_v_1_1_repository_1_1_i_vehicles_repository.html#aa7547cec8d4ec85113f8ce52e994533e", null ]
];