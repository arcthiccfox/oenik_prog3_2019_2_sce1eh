var namespace_b_k_v_1_1_logic =
[
    [ "Tests", "namespace_b_k_v_1_1_logic_1_1_tests.html", "namespace_b_k_v_1_1_logic_1_1_tests" ],
    [ "CoordLogic", "class_b_k_v_1_1_logic_1_1_coord_logic.html", "class_b_k_v_1_1_logic_1_1_coord_logic" ],
    [ "DriverLogic", "class_b_k_v_1_1_logic_1_1_driver_logic.html", "class_b_k_v_1_1_logic_1_1_driver_logic" ],
    [ "ICoordLogic", "interface_b_k_v_1_1_logic_1_1_i_coord_logic.html", "interface_b_k_v_1_1_logic_1_1_i_coord_logic" ],
    [ "IDriverLogic", "interface_b_k_v_1_1_logic_1_1_i_driver_logic.html", "interface_b_k_v_1_1_logic_1_1_i_driver_logic" ],
    [ "IInspectorLogic", "interface_b_k_v_1_1_logic_1_1_i_inspector_logic.html", "interface_b_k_v_1_1_logic_1_1_i_inspector_logic" ],
    [ "InspectorLogic", "class_b_k_v_1_1_logic_1_1_inspector_logic.html", "class_b_k_v_1_1_logic_1_1_inspector_logic" ],
    [ "IStationLogic", "interface_b_k_v_1_1_logic_1_1_i_station_logic.html", "interface_b_k_v_1_1_logic_1_1_i_station_logic" ],
    [ "IVehicleLogic", "interface_b_k_v_1_1_logic_1_1_i_vehicle_logic.html", "interface_b_k_v_1_1_logic_1_1_i_vehicle_logic" ],
    [ "IVTypesLogic", "interface_b_k_v_1_1_logic_1_1_i_v_types_logic.html", "interface_b_k_v_1_1_logic_1_1_i_v_types_logic" ],
    [ "NonCrudOperationLogic", "class_b_k_v_1_1_logic_1_1_non_crud_operation_logic.html", "class_b_k_v_1_1_logic_1_1_non_crud_operation_logic" ],
    [ "StationLogic", "class_b_k_v_1_1_logic_1_1_station_logic.html", "class_b_k_v_1_1_logic_1_1_station_logic" ],
    [ "VehicleLogic", "class_b_k_v_1_1_logic_1_1_vehicle_logic.html", "class_b_k_v_1_1_logic_1_1_vehicle_logic" ],
    [ "VTypeLogic", "class_b_k_v_1_1_logic_1_1_v_type_logic.html", "class_b_k_v_1_1_logic_1_1_v_type_logic" ]
];