var class_b_k_v_1_1_data_1_1coords_and_dispatchers =
[
    [ "coord_age", "class_b_k_v_1_1_data_1_1coords_and_dispatchers.html#afbfda1eb1cc826437730e7ffa69bce07", null ],
    [ "coord_avgHoursPerWeek", "class_b_k_v_1_1_data_1_1coords_and_dispatchers.html#a1456fc2914f3f4742c63586afdd49ded", null ],
    [ "coord_hiredDate", "class_b_k_v_1_1_data_1_1coords_and_dispatchers.html#ad2e72a5da1b2a0edfbd2c07336153912", null ],
    [ "coord_hoursWorkedTotal", "class_b_k_v_1_1_data_1_1coords_and_dispatchers.html#a2aae2f08c18ab008c9385671427e205d", null ],
    [ "coord_id", "class_b_k_v_1_1_data_1_1coords_and_dispatchers.html#ae46555d297c226ba587025e8ba5c1c1f", null ],
    [ "coord_name", "class_b_k_v_1_1_data_1_1coords_and_dispatchers.html#aa9b9fea94e89af6d2992109d02c784bf", null ],
    [ "coord_stationId", "class_b_k_v_1_1_data_1_1coords_and_dispatchers.html#a6f849e232fbb6346df46c3c38d52d371", null ],
    [ "stations", "class_b_k_v_1_1_data_1_1coords_and_dispatchers.html#a5a354040afdfaa0617f0742353c82664", null ]
];