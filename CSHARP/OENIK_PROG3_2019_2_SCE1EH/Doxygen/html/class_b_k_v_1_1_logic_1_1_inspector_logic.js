var class_b_k_v_1_1_logic_1_1_inspector_logic =
[
    [ "InspectorLogic", "class_b_k_v_1_1_logic_1_1_inspector_logic.html#ad38399e7cb2271ef579d3cc9fe13719a", null ],
    [ "InspectorLogic", "class_b_k_v_1_1_logic_1_1_inspector_logic.html#a9ea636a790996856d4affdedb85f661e", null ],
    [ "CreateInspector", "class_b_k_v_1_1_logic_1_1_inspector_logic.html#a04ae7b75cf21b60ba728b70345679f86", null ],
    [ "DeleteInspector", "class_b_k_v_1_1_logic_1_1_inspector_logic.html#af407738da4dc9f2f04cd2d47d4d39029", null ],
    [ "GetAll", "class_b_k_v_1_1_logic_1_1_inspector_logic.html#a3e993a0bd3168a1c76d3503de74ebf3c", null ],
    [ "ReadInspector", "class_b_k_v_1_1_logic_1_1_inspector_logic.html#acc0bad811fdfd059ca0c5551b9b95541", null ],
    [ "UpdateInspector", "class_b_k_v_1_1_logic_1_1_inspector_logic.html#a5f65c7d23816dab0615c21a3eb822745", null ]
];