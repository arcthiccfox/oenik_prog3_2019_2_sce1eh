var interface_b_k_v_1_1_logic_1_1_i_driver_logic =
[
    [ "CreateDriver", "interface_b_k_v_1_1_logic_1_1_i_driver_logic.html#ad934ea0a68d3ba6873faa249b24671c0", null ],
    [ "DeleteDriver", "interface_b_k_v_1_1_logic_1_1_i_driver_logic.html#a41cddb7ae39f45d7f614ac3b5ee3fa17", null ],
    [ "GetAll", "interface_b_k_v_1_1_logic_1_1_i_driver_logic.html#ac310f0f136b77778f6063b5f72457bf0", null ],
    [ "GetOneDriver", "interface_b_k_v_1_1_logic_1_1_i_driver_logic.html#aba0cf1a0a75f84ccd1773f7698f905f3", null ],
    [ "UpdateDriver", "interface_b_k_v_1_1_logic_1_1_i_driver_logic.html#a4ea3cf11d9ffeb873e7fe5c99405e6ef", null ]
];