var class_b_k_v_1_1_logic_1_1_driver_logic =
[
    [ "DriverLogic", "class_b_k_v_1_1_logic_1_1_driver_logic.html#ab8a269e444d8b19601358d98a78932b4", null ],
    [ "DriverLogic", "class_b_k_v_1_1_logic_1_1_driver_logic.html#a9e66322d2374499ea85283870023cc4e", null ],
    [ "CreateDriver", "class_b_k_v_1_1_logic_1_1_driver_logic.html#a7fe8ffb2d98d0078e1214b6a15059aaf", null ],
    [ "DeleteDriver", "class_b_k_v_1_1_logic_1_1_driver_logic.html#a1a3199321b2d0ced71e4bd8792b7e21e", null ],
    [ "GetAll", "class_b_k_v_1_1_logic_1_1_driver_logic.html#a0d20351129c3aa851d396d1f7af5a51e", null ],
    [ "GetOneDriver", "class_b_k_v_1_1_logic_1_1_driver_logic.html#aecae3e8ba4a728cfc3b180ff6614eb0b", null ],
    [ "UpdateDriver", "class_b_k_v_1_1_logic_1_1_driver_logic.html#aaa33193d9854fa8a7a35466267bdf215", null ]
];