var interface_b_k_v_1_1_logic_1_1_i_inspector_logic =
[
    [ "CreateInspector", "interface_b_k_v_1_1_logic_1_1_i_inspector_logic.html#aebb4331bf9233d165b4f6c9b23cbcdd8", null ],
    [ "DeleteInspector", "interface_b_k_v_1_1_logic_1_1_i_inspector_logic.html#a76b38e81a687f6b48f2ff4a964803eb6", null ],
    [ "GetAll", "interface_b_k_v_1_1_logic_1_1_i_inspector_logic.html#a4463ccb18c0431781ccde0107b7ef703", null ],
    [ "ReadInspector", "interface_b_k_v_1_1_logic_1_1_i_inspector_logic.html#aea889be706ceecdd8720aa0a46dabf7c", null ],
    [ "UpdateInspector", "interface_b_k_v_1_1_logic_1_1_i_inspector_logic.html#a92dc196ff3f6bbd5219bd87f5e416b0b", null ]
];