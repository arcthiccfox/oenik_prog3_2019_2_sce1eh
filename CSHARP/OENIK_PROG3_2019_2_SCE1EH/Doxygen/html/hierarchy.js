var hierarchy =
[
    [ "BKV.Data.coordsAndDispatchers", "class_b_k_v_1_1_data_1_1coords_and_dispatchers.html", null ],
    [ "BKV.Data.Data", "class_b_k_v_1_1_data_1_1_data.html", null ],
    [ "DbContext", null, [
      [ "BKV::Data::BKVDatabaseEntities", "class_b_k_v_1_1_data_1_1_b_k_v_database_entities.html", null ]
    ] ],
    [ "BKV.Data.drivers", "class_b_k_v_1_1_data_1_1drivers.html", null ],
    [ "BKV.Logic.ICoordLogic", "interface_b_k_v_1_1_logic_1_1_i_coord_logic.html", [
      [ "BKV.Logic.CoordLogic", "class_b_k_v_1_1_logic_1_1_coord_logic.html", null ]
    ] ],
    [ "BKV.Logic.IDriverLogic", "interface_b_k_v_1_1_logic_1_1_i_driver_logic.html", [
      [ "BKV.Logic.DriverLogic", "class_b_k_v_1_1_logic_1_1_driver_logic.html", null ]
    ] ],
    [ "BKV.Logic.IInspectorLogic", "interface_b_k_v_1_1_logic_1_1_i_inspector_logic.html", [
      [ "BKV.Logic.InspectorLogic", "class_b_k_v_1_1_logic_1_1_inspector_logic.html", null ]
    ] ],
    [ "BKV.Data.inspectors", "class_b_k_v_1_1_data_1_1inspectors.html", null ],
    [ "BKV.Repository.IRepository< T >", "interface_b_k_v_1_1_repository_1_1_i_repository.html", [
      [ "BKV.Repository.Repository< T >", "class_b_k_v_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "BKV.Repository.IRepository< coordsAndDispatchers >", "interface_b_k_v_1_1_repository_1_1_i_repository.html", [
      [ "BKV.Repository.ICoordsRepo", "interface_b_k_v_1_1_repository_1_1_i_coords_repo.html", [
        [ "BKV.Repository.CoordsRepo", "class_b_k_v_1_1_repository_1_1_coords_repo.html", null ]
      ] ]
    ] ],
    [ "BKV.Repository.IRepository< drivers >", "interface_b_k_v_1_1_repository_1_1_i_repository.html", [
      [ "BKV.Repository.IDriverRepository", "interface_b_k_v_1_1_repository_1_1_i_driver_repository.html", [
        [ "BKV.Repository.DriverRepository", "class_b_k_v_1_1_repository_1_1_driver_repository.html", null ]
      ] ]
    ] ],
    [ "BKV.Repository.IRepository< inspectors >", "interface_b_k_v_1_1_repository_1_1_i_repository.html", [
      [ "BKV.Repository.IInspectorRepository", "interface_b_k_v_1_1_repository_1_1_i_inspector_repository.html", [
        [ "BKV.Repository.InspectorRepository", "class_b_k_v_1_1_repository_1_1_inspector_repository.html", null ]
      ] ]
    ] ],
    [ "BKV.Repository.IRepository< stations >", "interface_b_k_v_1_1_repository_1_1_i_repository.html", [
      [ "BKV.Repository.IStationRepository", "interface_b_k_v_1_1_repository_1_1_i_station_repository.html", [
        [ "BKV.Repository.StationRepository", "class_b_k_v_1_1_repository_1_1_station_repository.html", null ]
      ] ]
    ] ],
    [ "BKV.Repository.IRepository< vehicles >", "interface_b_k_v_1_1_repository_1_1_i_repository.html", [
      [ "BKV.Repository.IVehiclesRepository", "interface_b_k_v_1_1_repository_1_1_i_vehicles_repository.html", [
        [ "BKV.Repository.VehiclesRepository", "class_b_k_v_1_1_repository_1_1_vehicles_repository.html", null ]
      ] ]
    ] ],
    [ "BKV.Repository.IRepository< vTypes >", "interface_b_k_v_1_1_repository_1_1_i_repository.html", [
      [ "BKV.Repository.IVTypesRepository", "interface_b_k_v_1_1_repository_1_1_i_v_types_repository.html", [
        [ "BKV.Repository.VTypesRepository", "class_b_k_v_1_1_repository_1_1_v_types_repository.html", null ]
      ] ]
    ] ],
    [ "BKV.Logic.IStationLogic", "interface_b_k_v_1_1_logic_1_1_i_station_logic.html", [
      [ "BKV.Logic.StationLogic", "class_b_k_v_1_1_logic_1_1_station_logic.html", null ]
    ] ],
    [ "BKV.Logic.IVehicleLogic", "interface_b_k_v_1_1_logic_1_1_i_vehicle_logic.html", [
      [ "BKV.Logic.VehicleLogic", "class_b_k_v_1_1_logic_1_1_vehicle_logic.html", null ]
    ] ],
    [ "BKV.Logic.IVTypesLogic", "interface_b_k_v_1_1_logic_1_1_i_v_types_logic.html", [
      [ "BKV.Logic.VTypeLogic", "class_b_k_v_1_1_logic_1_1_v_type_logic.html", null ]
    ] ],
    [ "BKV.Logic.Tests.LogicTests", "class_b_k_v_1_1_logic_1_1_tests_1_1_logic_tests.html", null ],
    [ "BKV.Logic.NonCrudOperationLogic", "class_b_k_v_1_1_logic_1_1_non_crud_operation_logic.html", null ],
    [ "BKV.Program.Program", "class_b_k_v_1_1_program_1_1_program.html", null ],
    [ "BKV.Repository.Repository< coordsAndDispatchers >", "class_b_k_v_1_1_repository_1_1_repository.html", [
      [ "BKV.Repository.CoordsRepo", "class_b_k_v_1_1_repository_1_1_coords_repo.html", null ]
    ] ],
    [ "BKV.Repository.Repository< drivers >", "class_b_k_v_1_1_repository_1_1_repository.html", [
      [ "BKV.Repository.DriverRepository", "class_b_k_v_1_1_repository_1_1_driver_repository.html", null ]
    ] ],
    [ "BKV.Repository.Repository< inspectors >", "class_b_k_v_1_1_repository_1_1_repository.html", [
      [ "BKV.Repository.InspectorRepository", "class_b_k_v_1_1_repository_1_1_inspector_repository.html", null ]
    ] ],
    [ "BKV.Repository.Repository< stations >", "class_b_k_v_1_1_repository_1_1_repository.html", [
      [ "BKV.Repository.StationRepository", "class_b_k_v_1_1_repository_1_1_station_repository.html", null ]
    ] ],
    [ "BKV.Repository.Repository< vehicles >", "class_b_k_v_1_1_repository_1_1_repository.html", [
      [ "BKV.Repository.VehiclesRepository", "class_b_k_v_1_1_repository_1_1_vehicles_repository.html", null ]
    ] ],
    [ "BKV.Repository.Repository< vTypes >", "class_b_k_v_1_1_repository_1_1_repository.html", [
      [ "BKV.Repository.VTypesRepository", "class_b_k_v_1_1_repository_1_1_v_types_repository.html", null ]
    ] ],
    [ "BKV.Data.stations", "class_b_k_v_1_1_data_1_1stations.html", null ],
    [ "BKV.Data.vehicles", "class_b_k_v_1_1_data_1_1vehicles.html", null ],
    [ "BKV.Data.vTypes", "class_b_k_v_1_1_data_1_1v_types.html", null ]
];