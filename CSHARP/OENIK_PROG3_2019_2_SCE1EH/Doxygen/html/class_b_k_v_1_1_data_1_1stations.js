var class_b_k_v_1_1_data_1_1stations =
[
    [ "stations", "class_b_k_v_1_1_data_1_1stations.html#ac061f604715320ce85f41d105dce0012", null ],
    [ "coordsAndDispatchers", "class_b_k_v_1_1_data_1_1stations.html#a82b696857bc6e25080d3c80ba6086f1d", null ],
    [ "station_cassaAvailability", "class_b_k_v_1_1_data_1_1stations.html#a4da8c7b93a5461b27fe1c83ed5586da1", null ],
    [ "station_id", "class_b_k_v_1_1_data_1_1stations.html#a7e27414cb9c28bd59c41451c20f444e8", null ],
    [ "station_lastRepairsDate", "class_b_k_v_1_1_data_1_1stations.html#aaf0609e11b9448281f06775b63012895", null ],
    [ "station_name", "class_b_k_v_1_1_data_1_1stations.html#a7ef58aa23ecc7fa98eaa0581c9bfa810", null ],
    [ "station_personnelAmount", "class_b_k_v_1_1_data_1_1stations.html#a7069d9bcac624aee5d8a9ad630bfe96c", null ],
    [ "station_ticketMachineAvailability", "class_b_k_v_1_1_data_1_1stations.html#acb2361ab15c5a90672dc629a2a848969", null ]
];