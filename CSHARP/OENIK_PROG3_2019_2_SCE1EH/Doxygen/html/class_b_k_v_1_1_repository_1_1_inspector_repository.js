var class_b_k_v_1_1_repository_1_1_inspector_repository =
[
    [ "InspectorRepository", "class_b_k_v_1_1_repository_1_1_inspector_repository.html#aa56a9c641d37ad2025b49e1ba2c94225", null ],
    [ "CreateInspector", "class_b_k_v_1_1_repository_1_1_inspector_repository.html#a807f1b1135e7fc7a97c0a530132ad89e", null ],
    [ "DeleteInspector", "class_b_k_v_1_1_repository_1_1_inspector_repository.html#a9b43b66b3af80911fbd8f6515cda801e", null ],
    [ "GetOne", "class_b_k_v_1_1_repository_1_1_inspector_repository.html#ac685fb273f2126bce05bbd91e2827d6a", null ],
    [ "ReadInspector", "class_b_k_v_1_1_repository_1_1_inspector_repository.html#a59648ca198fc220c5318ae775459f34f", null ],
    [ "UpdateInspector", "class_b_k_v_1_1_repository_1_1_inspector_repository.html#ad1cf5c73b9f956a3c86c9f681fda0598", null ]
];