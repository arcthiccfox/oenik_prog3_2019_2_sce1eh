var annotated_dup =
[
    [ "BKV", null, [
      [ "Data", null, [
        [ "BKVDatabaseEntities", "class_b_k_v_1_1_data_1_1_b_k_v_database_entities.html", null ],
        [ "coordsAndDispatchers", "class_b_k_v_1_1_data_1_1coords_and_dispatchers.html", "class_b_k_v_1_1_data_1_1coords_and_dispatchers" ],
        [ "Data", "class_b_k_v_1_1_data_1_1_data.html", null ],
        [ "drivers", "class_b_k_v_1_1_data_1_1drivers.html", "class_b_k_v_1_1_data_1_1drivers" ],
        [ "inspectors", "class_b_k_v_1_1_data_1_1inspectors.html", "class_b_k_v_1_1_data_1_1inspectors" ],
        [ "stations", "class_b_k_v_1_1_data_1_1stations.html", "class_b_k_v_1_1_data_1_1stations" ],
        [ "vehicles", "class_b_k_v_1_1_data_1_1vehicles.html", "class_b_k_v_1_1_data_1_1vehicles" ],
        [ "vTypes", "class_b_k_v_1_1_data_1_1v_types.html", "class_b_k_v_1_1_data_1_1v_types" ]
      ] ],
      [ "Logic", "namespace_b_k_v_1_1_logic.html", "namespace_b_k_v_1_1_logic" ],
      [ "Program", "namespace_b_k_v_1_1_program.html", "namespace_b_k_v_1_1_program" ],
      [ "Repository", "namespace_b_k_v_1_1_repository.html", "namespace_b_k_v_1_1_repository" ]
    ] ]
];