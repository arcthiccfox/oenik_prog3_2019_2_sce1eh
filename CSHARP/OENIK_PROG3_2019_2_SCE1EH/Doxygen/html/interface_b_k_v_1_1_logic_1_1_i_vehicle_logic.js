var interface_b_k_v_1_1_logic_1_1_i_vehicle_logic =
[
    [ "CreateVehicle", "interface_b_k_v_1_1_logic_1_1_i_vehicle_logic.html#a0b1f5dc7c745bcd535c80a43850d6b14", null ],
    [ "DeleteVehicle", "interface_b_k_v_1_1_logic_1_1_i_vehicle_logic.html#aedc85a93fea52bd09239c3a05aaa249f", null ],
    [ "GetAll", "interface_b_k_v_1_1_logic_1_1_i_vehicle_logic.html#acac0e6ebcdb291d2d805406e5019875b", null ],
    [ "ReadVehicle", "interface_b_k_v_1_1_logic_1_1_i_vehicle_logic.html#ab5da85d03d114f71a9800dd91ac31f61", null ],
    [ "UpdateVehicle", "interface_b_k_v_1_1_logic_1_1_i_vehicle_logic.html#a8435451281ccb90cbecaa551e071c86e", null ]
];