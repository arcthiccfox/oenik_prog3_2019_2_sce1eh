var class_b_k_v_1_1_logic_1_1_coord_logic =
[
    [ "CoordLogic", "class_b_k_v_1_1_logic_1_1_coord_logic.html#abbd2633bbf3a3320fe62c306ffd8e946", null ],
    [ "CoordLogic", "class_b_k_v_1_1_logic_1_1_coord_logic.html#afa1089128638de7ae83f7bcc5fefb654", null ],
    [ "CreateCoord", "class_b_k_v_1_1_logic_1_1_coord_logic.html#a7bad9acbf3f27e36be9481acd62ce24e", null ],
    [ "DeleteCoord", "class_b_k_v_1_1_logic_1_1_coord_logic.html#aa922e8a16c9d818f471f6e2d42fb5d0d", null ],
    [ "GetAll", "class_b_k_v_1_1_logic_1_1_coord_logic.html#a4204c3972af999d229fdfb3c86bbe0d7", null ],
    [ "ReadCoord", "class_b_k_v_1_1_logic_1_1_coord_logic.html#a66e8dbc7f88af73f62e11a95a8841b1a", null ],
    [ "UpdateCoord", "class_b_k_v_1_1_logic_1_1_coord_logic.html#a732a3b30aa9fd01aa0690b0a1eb59c12", null ]
];