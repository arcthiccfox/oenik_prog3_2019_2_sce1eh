var interface_b_k_v_1_1_repository_1_1_i_inspector_repository =
[
    [ "CreateInspector", "interface_b_k_v_1_1_repository_1_1_i_inspector_repository.html#ab2d0d5f9029554335d0bd526f4068e6a", null ],
    [ "DeleteInspector", "interface_b_k_v_1_1_repository_1_1_i_inspector_repository.html#a08e8250d77f6186b749b64e33903bb5f", null ],
    [ "GetAll", "interface_b_k_v_1_1_repository_1_1_i_inspector_repository.html#ad389b419914f8222c54b4db5b509c4ee", null ],
    [ "ReadInspector", "interface_b_k_v_1_1_repository_1_1_i_inspector_repository.html#adab517da345694b849331fe46098d099", null ],
    [ "UpdateInspector", "interface_b_k_v_1_1_repository_1_1_i_inspector_repository.html#a87ddd9bf6e5afc2d8dea05d7bf62ea6d", null ]
];