var class_b_k_v_1_1_data_1_1inspectors =
[
    [ "inspector_age", "class_b_k_v_1_1_data_1_1inspectors.html#a02c05f82103f822d71b6a699171aead2", null ],
    [ "inspector_avgHoursPerWeek", "class_b_k_v_1_1_data_1_1inspectors.html#af0c19e82f25ceeb54fec5b75263bd7e8", null ],
    [ "inspector_hiredDate", "class_b_k_v_1_1_data_1_1inspectors.html#aa36d661049dba822d3ad26886ba79493", null ],
    [ "inspector_hoursWorkedTotal", "class_b_k_v_1_1_data_1_1inspectors.html#a71b54aa8de56b70597f488a2fdef91b8", null ],
    [ "inspector_id", "class_b_k_v_1_1_data_1_1inspectors.html#ac64f542c3421201774c4567fc835724a", null ],
    [ "inspector_lastInspectedVehicleId", "class_b_k_v_1_1_data_1_1inspectors.html#a4dfe0c8167a16eeb093a70a042a18f95", null ],
    [ "inspector_name", "class_b_k_v_1_1_data_1_1inspectors.html#af711e0849dab2dc8bfa9fd96cd7355ad", null ],
    [ "vehicles", "class_b_k_v_1_1_data_1_1inspectors.html#a3229f1f5c00f9d2d0855f0bee2cc384f", null ]
];