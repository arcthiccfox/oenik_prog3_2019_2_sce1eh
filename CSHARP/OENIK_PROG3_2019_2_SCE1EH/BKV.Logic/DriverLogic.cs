﻿namespace BKV.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;
    using BKV.Repository;

    /// <summary>
    /// Driverlogic.
    /// </summary>
    public class DriverLogic : IDriverLogic
    {
        private IDriverRepository driverRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="DriverLogic"/> class.
        /// </summary>
        /// <param name="entities">Entity framework instance.</param>
        public DriverLogic(BKVDatabaseEntities entities)
        {
            // this.driverRepo = new DriverRepository(new BKVDatabaseEntities());
            this.driverRepo = new DriverRepository(entities);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DriverLogic"/> class.
        /// </summary>
        /// <param name="driverRepo">IDriverRepository type.</param>
        public DriverLogic(IDriverRepository driverRepo)
        {
            this.driverRepo = driverRepo;
        }

        /*public DriverLogic(IDriverRepository driverRepo) // inject Interface from outside. create factory ?
        {
            this.driverRepo = driverRepo;
        }*/

        /// <summary>
        /// Creates a driver, refers to the repo.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="name">name.</param>
        /// <param name="age">age.</param>
        /// <param name="hiredDate">the date employee was hired.</param>
        /// <param name="avgHoursPerWeek">average hrs worked.</param>
        /// <param name="hoursWorkedTotal">total global hours worked.</param>
        /// <param name="lastDrivenVehicleID">the unique id of the vehicle last used in work.</param>
        public void CreateDriver(int id, string name, int age, DateTime hiredDate, int avgHoursPerWeek, int hoursWorkedTotal, int lastDrivenVehicleID)
        {
            if (this.driverRepo.GetOne(id) == null && age > 14 && age < 150 && hiredDate <= DateTime.Now)
            {
                this.driverRepo.CreateDriver(id, name, age, hiredDate, avgHoursPerWeek, hoursWorkedTotal, lastDrivenVehicleID);
            }
        }

        /// <summary>
        /// Deletes driver.
        /// </summary>
        /// <param name="id">Primary key.</param>
        public void DeleteDriver(int id)
        {
            if (this.driverRepo.GetOne(id) != null) // Stop trying to delete what's already not there
            {
                this.driverRepo.DeleteDriver(id);
            }
        }

        /// <summary>
        /// Gets all the drivers from repo.
        /// </summary>
        /// <returns>IQueryable<drivers>.</returns>
        public IQueryable<drivers> GetAll()
        {
            return this.driverRepo.GetAll();
        }

        /// <summary>
        /// Gets one driver from repo.
        /// </summary>
        /// <param name="id">Primary key.</param>
        /// <returns>A single driver instance.</returns>
        public drivers GetOneDriver(int id)
        {
            return this.driverRepo.GetOne(id);
        }

        /// <summary>
        /// Changes data of a single driver instance.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="name">name.</param>
        /// <param name="age">age.</param>
        /// <param name="hiredDate">the date employee was hired.</param>
        /// <param name="avgHoursPerWeek">average hrs worked.</param>
        /// <param name="hoursWorkedTotal">total global hours worked.</param>
        /// <param name="lastDrivenVehicleID">the unique id of the vehicle last used in work.</param>
        public void UpdateDriver(int id, string name, int age, DateTime hiredDate, int avgHoursPerWeek, int hoursWorkedTotal, int lastDrivenVehicleID)
        {
            if ((this.driverRepo.GetOne(id) != null) && hiredDate <= DateTime.Now)
            {
                this.driverRepo.UpdateDriver(id, name, age, hiredDate, avgHoursPerWeek, hoursWorkedTotal, lastDrivenVehicleID);
            }
        }
    }
}
