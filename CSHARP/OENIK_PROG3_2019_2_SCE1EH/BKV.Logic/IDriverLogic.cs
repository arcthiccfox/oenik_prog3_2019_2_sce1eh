﻿namespace BKV.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;
    using BKV.Repository;

    public interface IDriverLogic
    {
        drivers GetOneDriver(int id);

        void CreateDriver(int id, string name, int age, DateTime hiredDate, int avgHoursPerWeek, int hoursWorkedTotal, int lastDrivenVehicleID);

        void UpdateDriver(int id, string name, int age, DateTime hiredDate, int avgHoursPerWeek, int hoursWorkedTotal, int lastDrivenVehicleID);

        void DeleteDriver(int id);

        IQueryable<drivers> GetAll();
    }
}
