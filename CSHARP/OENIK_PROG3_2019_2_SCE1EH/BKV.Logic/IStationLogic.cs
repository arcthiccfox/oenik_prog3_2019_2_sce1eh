﻿namespace BKV.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;

    interface IStationLogic
    {
        void CreateStation(int id, string name, int personnelAmount, bool cassaAvailability, bool ticketMachineAvailability, DateTime lastRepairsDate);

        stations ReadStation(int id);

        void UpdateStation(int id, string name, int personnelAmount, bool cassaAvailability, bool ticketMachineAvailability, DateTime lastRepairsDate);

        void RemoveStation(int id);

        IQueryable<stations> GetAll();
    }
}
