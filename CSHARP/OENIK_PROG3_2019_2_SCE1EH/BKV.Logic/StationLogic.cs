﻿namespace BKV.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;
    using BKV.Repository;
    /// <summary>
    /// This is the logic container for stations.
    /// </summary>
    public class StationLogic : IStationLogic
    {
        private IStationRepository stationRepo;

        public StationLogic(BKVDatabaseEntities entities)
        {
            this.stationRepo = new StationRepository(entities);
        }

        public StationLogic(IStationRepository stationRepo)
        {
            this.stationRepo = stationRepo;
        }

        public void CreateStation(int id, string name, int personnelAmount, bool cassaAvailability, bool ticketMachineAvailability, DateTime lastRepairsDate)
        {
            stationRepo.CreateStation(id, name, personnelAmount, cassaAvailability, ticketMachineAvailability, lastRepairsDate);
        }

        public IQueryable<stations> GetAll()
        {
            return stationRepo.GetAll();
        }

        public stations ReadStation(int id)
        {
            return stationRepo.ReadStation(id);
        }

        public void RemoveStation(int id)
        {
            stationRepo.DeleteStation(id);
        }

        public void UpdateStation(int id, string name, int personnelAmount, bool cassaAvailability, bool ticketMachineAvailability, DateTime lastRepairsDate)
        {
            stationRepo.UpdateStation(id, name, personnelAmount, cassaAvailability, ticketMachineAvailability, lastRepairsDate);
        }

    }
}
