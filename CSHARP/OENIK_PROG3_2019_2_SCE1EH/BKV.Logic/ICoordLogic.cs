﻿namespace BKV.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;
    using BKV.Repository;

    public interface ICoordLogic
    {
        coordsAndDispatchers ReadCoord(int id);

        void CreateCoord(int id, string name, int age, DateTime hiredDate, int avgHrsPerWeek, int hrsWorkedTotal, int stationId);

        void UpdateCoord(int id, string name, int age, DateTime hiredDate, int avgHrsPerWeek, int hrsWorkedTotal, int stationId);

        void DeleteCoord(int id);

        IQueryable<coordsAndDispatchers> GetAll();
    }
}
