﻿namespace BKV.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;
    using BKV.Repository;

    /// <summary>
    /// This contains logic for all the vehicle types.
    /// </summary>
    public class VTypeLogic : IVTypesLogic
    {
        private IVTypesRepository vTypeRepo;

        public VTypeLogic()
        {
            this.vTypeRepo = new VTypesRepository(new BKVDatabaseEntities());
        }

        public void CreateVType(int id, string typeName, string modelName, int maxSpeed, int maxPassangers, string fuelType)
        {
            vTypeRepo.CreateVType(id, typeName, modelName, maxSpeed, maxPassangers, fuelType);
        }

        public void DeleteVType(int id)
        {
            vTypeRepo.DeleteVType(id);
        }

        public vTypes ReadVType(int id)
        {
            return vTypeRepo.ReadVType(id);
        }

        public void UpdateVType(int id, string typeName, string modelName, int maxSpeed, int maxPassangers, string fuelType)
        {
            vTypeRepo.UpdateVType(id, typeName, modelName, maxSpeed, maxPassangers, fuelType);
        }
    }
}
