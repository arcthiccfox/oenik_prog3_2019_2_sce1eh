﻿namespace BKV.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;
    using BKV.Repository;

    /// <summary>
    /// This contains all the available logic for vehicles.
    /// </summary>
    public class VehicleLogic : IVehicleLogic
    {
        private IVehiclesRepository vehicleRepo;

        public VehicleLogic(BKVDatabaseEntities entities)
        {
            this.vehicleRepo = new VehiclesRepository(entities);
        }

        public VehicleLogic(IVehiclesRepository vehicleRepo)
        {
            this.vehicleRepo = vehicleRepo;
        }

        public void CreateVehicle(int id, int typeId, string serialNum, string lineNum, DateTime dateManufactured, DateTime lastInspectedDate, int totalHrsDroven, DateTime lastRepairsDate)
        {
            this.vehicleRepo.CreateVehicle(id, typeId, serialNum, lineNum, dateManufactured, lastInspectedDate, totalHrsDroven, lastRepairsDate);
        }

        public void DeleteVehicle(int id)
        {
            this.vehicleRepo.DeleteVehicle(id);
        }

        public vehicles ReadVehicle(int id)
        {
            return this.ReadVehicle(id);
        }

        public void UpdateVehicle(int id, int typeId, string serialNum, string lineNum, DateTime dateManufactured, DateTime lastInspectedDate, int totalHrsDroven, DateTime lastRepairsDate)
        {
            this.vehicleRepo.UpdateVehicle(id, typeId, serialNum, lineNum, dateManufactured, lastInspectedDate, totalHrsDroven, lastRepairsDate);
        }

        public IQueryable<vehicles> GetAll()
        {
            return this.vehicleRepo.GetAll();
        }
    }
}
