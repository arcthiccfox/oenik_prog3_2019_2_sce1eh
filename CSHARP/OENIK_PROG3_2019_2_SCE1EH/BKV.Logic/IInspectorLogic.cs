﻿namespace BKV.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;

    public interface IInspectorLogic
    {
        void CreateInspector(int id, string name, int age, DateTime hiredDate, int avgHrsPerWeek, int hrsWorkedTotal, int lastInspectedVehicleId);

        inspectors ReadInspector(int id);

        void UpdateInspector(int id, string name, int age, DateTime hiredDate, int avgHrsPerWeek, int hrsWorkedTotal, int lastInspectedVehicleId);

        void DeleteInspector(int id);

        IQueryable<inspectors> GetAll();
    }
}
