﻿namespace BKV.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;
    using BKV.Repository;

    /// <summary>
    /// Logic of inspector employees.
    /// </summary>
    public class InspectorLogic : IInspectorLogic
    {
        private IInspectorRepository inspectorRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="InspectorLogic"/> class.
        /// </summary>
        /// <param name="entities">Entities</param>
        public InspectorLogic(BKVDatabaseEntities entities)
        {
            this.inspectorRepo = new InspectorRepository(entities);
        }

        public InspectorLogic(IInspectorRepository inspectorRepo)
        {
            this.inspectorRepo = inspectorRepo;
        }

        public void CreateInspector(int id, string name, int age, DateTime hiredDate, int avgHrsPerWeek, int hrsWorkedTotal, int lastInspectedVehicleId)
        {
            this.inspectorRepo.CreateInspector(id, name, age, hiredDate, avgHrsPerWeek, hrsWorkedTotal, lastInspectedVehicleId);
        }

        public void DeleteInspector(int id)
        {
            this.inspectorRepo.DeleteInspector(id);
        }

        public IQueryable<inspectors> GetAll()
        {
            return inspectorRepo.GetAll();
        }

        public inspectors ReadInspector(int id)
        {
            return this.ReadInspector(id);
        }

        public void UpdateInspector(int id, string name, int age, DateTime hiredDate, int avgHrsPerWeek, int hrsWorkedTotal, int lastInspectedVehicleId)
        {
            this.inspectorRepo.CreateInspector(id, name, age, hiredDate, avgHrsPerWeek, hrsWorkedTotal, lastInspectedVehicleId);
        }
    }
}
