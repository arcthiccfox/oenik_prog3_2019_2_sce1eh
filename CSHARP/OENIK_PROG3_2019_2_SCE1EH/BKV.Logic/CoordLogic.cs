﻿namespace BKV.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;
    using BKV.Repository;

    /// <summary>
    /// Logic of coordinator employees.
    /// </summary>
    public class CoordLogic : ICoordLogic
    {
        private ICoordsRepo coordRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="CoordLogic"/> class.
        /// </summary>
        /// <param name="entities">Entities</param>
        public CoordLogic(BKVDatabaseEntities entities)
        {
            this.coordRepo = new CoordsRepo(entities);
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="CoordLogic"/> class.
        /// </summary>
        /// <param name="coordRepo">Interface</param>
        public CoordLogic(ICoordsRepo coordRepo)
        {
            this.coordRepo = coordRepo;
        }

        public void CreateCoord(int id, string name, int age, DateTime hiredDate, int avgHrsPerWeek, int hrsWorkedTotal, int stationId)
        {
            coordRepo.CreateCoord(id, name, age, hiredDate, avgHrsPerWeek, hrsWorkedTotal, stationId);
        }

        public void DeleteCoord(int id)
        {
            coordRepo.DeleteCoord(id);
        }

        public IQueryable<coordsAndDispatchers> GetAll()
        {
            return this.coordRepo.GetAll();
        }

        public coordsAndDispatchers ReadCoord(int id)
        {
            return coordRepo.ReadCoord(id);
        }

        public void UpdateCoord(int id, string name, int age, DateTime hiredDate, int avgHrsPerWeek, int hrsWorkedTotal, int stationId)
        {
            coordRepo.UpdateCoord(id, name, age, hiredDate, avgHrsPerWeek, hrsWorkedTotal, stationId);
        }
    }
}
