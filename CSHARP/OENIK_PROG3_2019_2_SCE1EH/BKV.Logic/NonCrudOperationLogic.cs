﻿namespace BKV.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;

    /// <summary>
    /// A library of the three functions that carry out three non-crud operations.
    /// </summary>
    public class NonCrudOperationLogic
    {
        // (6) Get all the coordinators & dispatchers stationed at a specified station
        private CoordLogic coordLogic;

        private StationLogic stationLogic;

        private DriverLogic driverLogic;

        private VehicleLogic vehicleLogic;

        private InspectorLogic inspectorLogic;

        /// <summary>
        /// Gets coordinator located at the station, if there isnt any, returns null.
        /// </summary>
        /// <param name="coordLogic">CoordLogic</param>
        /// <param name="stationLogic">StationLogic</param>
        /// <param name="stationName">Station's name we want to check coordinators for</param>
        /// <returns>The coordinator at Ujpest-Kozpont. If does not exist, returns a coord null object.</returns>
        public List<coordsAndDispatchers> GetCoordinatorsAtStation(CoordLogic coordLogic, StationLogic stationLogic, string stationName)
        {
            string _stationName = stationName;

            List<coordsAndDispatchers> coords = new List<coordsAndDispatchers>();

            this.coordLogic = coordLogic;
            this.stationLogic = stationLogic;
            IQueryable<coordsAndDispatchers> myCoords = this.coordLogic.GetAll();
            IQueryable<stations> myStations = this.stationLogic.GetAll();
            int stationID = -1;
            coordsAndDispatchers coordAtStationNull = new coordsAndDispatchers();

            foreach (var coord in myCoords)
            {
                stationID = (int)coord.coord_stationId;
                foreach (var station in myStations)
                {
                    if (station.station_id == stationID && station.station_name == _stationName)
                    {
                        coords.Add(coord);
                    }
                    else
                    {
                        //return coordAtStationNull;
                    }
                }
            }

            return coords;
        }

        /// <summary>
        /// (7) Get driver whose last used vehicle had at least 5000 hours driven.
        /// </summary>
        /// <param name="driverLogic">driverlogic.</param>
        /// <param name="vehicleLogic">vehiclelogic.</param>
        /// <param name="hours">Total hours the vehicle has been in use.</param>
        /// <returns>A driver whose last driven vehicle has at least user input specified hours in use.</returns>
        public List<drivers> GetDriverWithVehicleDrivenForSpecificHours(DriverLogic driverLogic, VehicleLogic vehicleLogic, int hours)
        {
            this.driverLogic = driverLogic;
            this.vehicleLogic = vehicleLogic;

            List<drivers> drivers = new List<drivers>();

            IQueryable<drivers> myDrivers = this.driverLogic.GetAll();
            IQueryable<vehicles> myVehicles = this.vehicleLogic.GetAll();

            foreach (drivers myDriver in myDrivers)
            {
                int lastUsedVehicleID = (int)myDriver.driver_lastDrivenVehicleId;
                foreach (vehicles myVehicle in myVehicles)
                {
                    if (myVehicle.vehicle_id == myDriver.driver_lastDrivenVehicleId && myVehicle.vehicle_totalHoursDriven >= hours)
                    {
                        drivers.Add(myDriver);
                    }
                    else
                    {
                        //driverNull = myDriver;
                    }
                }
            }

            return drivers;
        }

        /// <summary>
        /// 8) Get ticket inspector whose last inspected vehicle was last repaired more than X years ago.
        /// </summary>
        /// <param name="inspectorLogic">inspectorlogic.</param>
        /// <param name="vehiclelogic">vehiclelogic.</param>
        /// <param name="time">The vehicle was last repaired before this date.</param>
        /// <returns>An inspector whose last vehicle was repaired before the date</returns>
        public List<inspectors> GetInspectorWhoseLastVehicleWasRepairedMoreThanSpecificTimeAgo(InspectorLogic inspectorLogic, VehicleLogic vehiclelogic, DateTime time)
        {
            inspectors inspector = new inspectors();
            this.inspectorLogic = inspectorLogic;
            this.vehicleLogic = vehiclelogic;
            List<inspectors> inspectorList = new List<inspectors>();

            IQueryable<inspectors> myInspectors = this.inspectorLogic.GetAll();
            IQueryable<vehicles> myVehicles = this.vehicleLogic.GetAll();

            foreach (inspectors myInspector in myInspectors)
            {
                int lastUsedVehicleID = (int)myInspector.inspector_lastInspectedVehicleId;
                foreach (vehicles myVehicle in myVehicles)
                {
                    if (myVehicle.vehicle_id == myInspector.inspector_lastInspectedVehicleId && myVehicle.vehicle_lastRepairsDate <= time)
                    {
                        inspectorList.Add(myInspector);
                    }
                    else
                    {
                    }
                }
            }

            return inspectorList;
        }
    }
}
