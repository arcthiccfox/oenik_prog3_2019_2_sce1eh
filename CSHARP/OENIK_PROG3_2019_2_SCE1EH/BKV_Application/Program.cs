﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BKV.Program
{
    using System;
    using System.Collections.Generic;
    using System.Json;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using BKV.Data;
    using BKV.Logic;

    /// <summary>
    /// Contains the entry function Main().
    /// </summary>
    internal class Program
    {/// <summary>
    ///  The entry function that executes the console menu.
    /// </summary>
    /// <param name="args">Is the default Main() args.</param>
        protected static void Main(string[] args) // Prepare for ugly, redundant code! Lots of blocks could be made better
        {// Told myself I was gonna refactor this app. How foolish I truly was... Legacy code always survives
            DriverLogic driverLogic = new DriverLogic(new BKVDatabaseEntities());
            VehicleLogic vehicleLogic = new VehicleLogic(new BKVDatabaseEntities());
            InspectorLogic inspectorLogic = new InspectorLogic(new BKVDatabaseEntities());
            CoordLogic coordLogic = new CoordLogic(new BKVDatabaseEntities());
            StationLogic stationLogic = new StationLogic(new BKVDatabaseEntities());

            NonCrudOperationLogic nonCrud = new NonCrudOperationLogic();

            bool userWantsToQuit = false;

            string userChoice = string.Empty;

            int PK = 100;                   // for manual test data
            int FK = 1;                     // all employees will be assigned to the same vehicle for now.

            List<int> excludedPKs = new List<int>();

            while (!userWantsToQuit)
            {
                Console.Clear();
                Console.WriteLine($"Welcome to The Revolutionary BKV Database UI! (Proudly developed with {GetiGNumber()}iG[tm] NyrtKftZrt & Opossum Local[tm] (un)ltd." +
                    "\nPlease select a functionality by typing its corresponding number:\n" +
                    "\n(1) Create a driver" +
                    "\n(2) Read a driver (by ID/Primary key)" +
                    "\n(3) Update a driver" +
                    "\n(4) Delete a driver\n" +
                    "\n(5) Query a driver's data using our high-tech proprietary JAVA end-point (experimental feature)" +
                    "\n\n(6) Get all the coordinator / dispatcher stationed at Újpest-Központ" +
                    "\n(7) Get all the drivers whose last used vehicle had at least 5000 hours driven" +
                    "\n(8) Get all the ticket inspectors whose last inspected vehicle was last repaired more than X years ago" +
                    "\n\n(9) Get all the drivers in the table" +
                    "\n\n(Q) Quit the Revolutionary BKV Database Program");
                Console.Write("\nMenu item of your choice: ");
                userChoice = Console.ReadLine().ToLower();

                if (userChoice == "q")
                {
                    userWantsToQuit = true;
                }// same efficiency as case but more ugly lol
                else if (userChoice == "1")
                {
                    Console.Clear();
                    Console.WriteLine("Type the name of the employee: ");
                    string name = Console.ReadLine();
                    Console.Clear();
                    Console.WriteLine("Type age of employee: ");
                    int age = int.Parse(Console.ReadLine());
                    if (age < 14 || age > 125)
                    {
                        Console.WriteLine("Age has to be between 14 and 125");
                        Console.ReadLine();
                        continue;
                    }

                    Console.Clear();

                    Console.WriteLine("Working hours per week for the employee:");
                    int weekHrs = int.Parse(Console.ReadLine());
                    if (weekHrs < 0 || weekHrs > 168)
                    {
                        Console.Clear();
                        Console.WriteLine("Impossible.");
                        Console.ReadLine();
                        Console.Clear();
                        continue;
                    }
                    Console.Clear();
                    Console.WriteLine("Creating driver . . .");
                    driverLogic.CreateDriver(PK + 0, name, age, DateTime.Now.AddYears(-1), weekHrs, weekHrs * 52, FK); // by default all employees worked 1 year & all employees work on the same vehicle for simplicity
                    Console.Clear();

                    // Console.WriteLine("creating 2 hard-coded employees . . . ");
                    // driverLogic.CreateDriver(PK + 0, "joe_mama", 37, DateTime.Now, 40, 2000, FK);
                    // driverLogic.CreateDriver(PK + 1, "yuri", 37, DateTime.Now, 40, 2000, FK);
                    // Console.WriteLine("continue not skipped");
                    // Console.WriteLine("2 hard-coded employees created. Press return.");
                    Console.Write("Primary key for your driver created: ");
                    Console.Write(PK);
                    PK++;
                    Console.ReadLine();
                }
                else if (userChoice == "2")
                {
                    Console.Clear();
                    Console.WriteLine("Type primary key of employee: ");
                    int chosen = int.Parse(Console.ReadLine());
                    if (chosen > 999 || chosen >= PK || chosen < 100)
                    {
                        Console.Clear();
                        StringBuilder SB1 = new StringBuilder();
                        SB1
                            .Append("Between 100 and ")
                            .Append(PK.ToString())
                            .Append(" allowed.");
                        Console.WriteLine(SB1.ToString());
                        Console.ReadLine();
                        continue;
                    }

                    Console.Clear();
                    Console.WriteLine("Querying your driver...");

                    drivers driver1 = driverLogic.GetOneDriver(chosen);

                    StringBuilder SB = new StringBuilder();
                    SB
                        .Append("PK: ")
                        .Append(driver1.driver_id.ToString())
                        .Append(" Name: ")
                        .Append(driver1.driver_name.ToString())
                        .Append(" Date hired: ")
                        .Append(driver1.driver_hiredDate.ToString())
                        .Append(" Average hours a week: ".ToString())
                        .Append(driver1.driver_avgHoursPerWeek.ToString());

                    Console.Clear();
                    Console.WriteLine(SB.ToString());

                    Console.ReadLine();
                }
                else if (userChoice == "3")
                {
                    Console.Clear();
                    Console.Write("Type in primary key of driver to be updated: ");
                    int chosen = int.Parse(Console.ReadLine());
                    if (chosen < 0 || chosen > 999 || chosen >= PK || chosen < 100)
                    {
                        Console.Clear();
                        StringBuilder SB1 = new StringBuilder();
                        SB1
                            .Append("Between 100 and ")
                            .Append(PK.ToString())
                            .Append(" allowed.");
                        Console.WriteLine(SB1.ToString());
                        Console.ReadLine();
                        continue;
                    }

                    Console.Clear();
                    Console.WriteLine("Type the new name of the employee: ");
                    string name = Console.ReadLine();
                    Console.Clear();
                    Console.WriteLine("Type new age of employee: ");
                    int age = int.Parse(Console.ReadLine());
                    if (age < 14 || age > 125)
                    {
                        Console.WriteLine("Age has to be between 14 and 125");
                        Console.ReadLine();
                        continue;
                    }

                    Console.Clear();

                    Console.WriteLine("New working hours per week for the employee:");
                    int weekHrs = int.Parse(Console.ReadLine());
                    if (weekHrs < 0 || weekHrs > 168)
                    {
                        Console.Clear();
                        Console.WriteLine("Impossible.");
                        Console.ReadLine();
                        Console.Clear();
                        continue;
                    }

                    Console.WriteLine("Updating driver . . .");
                    driverLogic.UpdateDriver(chosen, name, age, DateTime.Now.AddYears(-1), weekHrs, weekHrs * 52, FK); // I know it will update date_hired, etc...
                    Console.Clear();
                    Console.WriteLine("Driver updated! Press return.");
                }
                else if (userChoice == "4")
                {
                    Console.Clear();
                    Console.Write("Type in primary key of driver to be deleted: ");
                    int chosen = int.Parse(Console.ReadLine());
                    if (chosen < 0 || chosen > 999 || chosen >= PK || chosen < 100)
                    {
                        Console.Clear();
                        StringBuilder SB1 = new StringBuilder();
                        SB1
                            .Append("Between 100 and ")
                            .Append(PK.ToString())
                            .Append(" allowed.");
                        Console.WriteLine(SB1.ToString());
                        Console.ReadLine();
                        continue;
                    }

                    Console.Clear();
                    Console.WriteLine("Deleting driver ...");
                    try
                    {
                        driverLogic.DeleteDriver(chosen);
                    }

                    // if Driver does not exist!
                    catch (InvalidOperationException e)
                    {
                        Console.Clear();
                        Console.WriteLine(e.Message);
                        Console.ReadLine();
                    }

                    Console.Clear();
                    Console.WriteLine("Driver deleted! Press return.");
                    Console.ReadLine();
                    excludedPKs.Add(chosen);
                }
                else if (userChoice == "5")
                {
                    Console.Clear();
                    WebClient wc = new WebClient();
                    string content;
                    try
                    {
                        Console.WriteLine("Connecting to servlet . . .");
                        content = string.Empty;
                        content += wc.DownloadString("http://localhost:8080/BKV_JavaEndPoint/BKVServlet");
                        Console.WriteLine(content);
                    }
                    catch (System.Net.WebException e)
                    {
                        Console.Clear();
                        Console.WriteLine(e.Message);
                    }

                    Console.ReadLine();
                    Console.Clear();
                }
                else if (userChoice == "6")
                {
                    Console.Clear();
                    Console.WriteLine("Loading data . . .");
                    StringBuilder sb = new StringBuilder();
                    List<coordsAndDispatchers> coords = nonCrud.GetCoordinatorsAtStation(coordLogic, stationLogic, "Újpest-Központ");
                    sb
                        .Append("Name\t\tCoordinator / dispatcher stationed at Újpest-Központ:")
                        .Append("\n\n");
                    foreach (var item in coords)
                    {
                         sb.Append(item.coord_name)
                        .Append("\t");
                    }

                    Console.Clear();
                    Console.WriteLine(sb.ToString());
                    Console.ReadLine();
                }
                else if (userChoice == "7")
                {
                    Console.Clear();
                    Console.Write("Loading data . . .");
                    StringBuilder sb = new StringBuilder();
                    List<drivers> drivers = nonCrud.GetDriverWithVehicleDrivenForSpecificHours(driverLogic, vehicleLogic, 5000);
                    sb
                        .Append("Name\t\tDriver with last used vehicles with a mileage of more than 5000 hours\n");
                    foreach (var item in drivers)
                    {
                        sb.Append(item.driver_name)
                        .Append("\n");
                    }

                    Console.Clear();
                    Console.WriteLine(sb.ToString());
                    Console.ReadLine();
                }
                else if (userChoice == "8")
                {
                    Console.Clear();
                    Console.Write("Loading data . . .");
                    int years = 3;
                    years = years * -1;
                    StringBuilder sb = new StringBuilder();
                    sb.Append($"Name\t\tInspector with a vehicle last repaired more than {years*-1} year(s) ago:\n");
                    List<inspectors> inspectors = nonCrud.GetInspectorWhoseLastVehicleWasRepairedMoreThanSpecificTimeAgo(inspectorLogic, vehicleLogic, DateTime.Now.AddYears(years));
                    foreach (var item in inspectors)
                    {
                        sb
                            .Append(item.inspector_name)
                            .Append("\t")
                            .Append("\n");
                    }
                    Console.Clear();
                    Console.WriteLine(sb.ToString());
                    Console.ReadLine();
                }
                else if (userChoice == "9")
                {
                    Console.Clear();
                    Console.Write("Loading data . . .");
                    StringBuilder sb = new StringBuilder();
                    sb
                      .Append("Name\t\tID / Primary key")
                      .Append("\n\n");
                    foreach (var item in driverLogic.GetAll())
                    {
                        sb
                            .Append(item.driver_name)
                            .Append("\t")
                            .Append(item.driver_id.ToString())
                            .Append("\n");
                    }

                    Console.Clear();
                    Console.Write(sb.ToString());
                    Console.ReadLine();
                }
                else if (userChoice != string.Empty)
                {
                    Console.Clear();
                    Console.Write("Invalid menu choice.\nWould you like to go back to the menu? (y/n): ");
                    userChoice = Console.ReadLine().ToLower();
                    if (userChoice == "n" || userChoice == "q")
                    {
                        userWantsToQuit = true;
                    }
                }
            }

            Console.Clear();
            Console.WriteLine($"Thank you for using The Revolutionary Nemzeti BKV Database Application.\nYou may send suggestions & complaints to 1037 Budapest, Monotonvideós street {GetStreetNumber()}.");
            Console.ReadKey();
        }

        /// <summary>
        /// Generates a number between 1-9, excluding 4.
        /// </summary>
        /// <returns>An integer between 1-9 excluding 4.</returns>
        protected static int GetiGNumber()
        {
            Random igNumberRandomize = new Random();
            int iGNumber = 0;
            do
            {
                iGNumber = igNumberRandomize.Next(1, 10);
            }
            while (iGNumber == 4);
            return iGNumber;
        }

        /// <summary>
        /// Generates a random street number (ie. 28/B) for the goodbye message upon quitting the console UI.
        /// </summary>
        /// <returns>A string which is formatted as: streetNumber/letter, where streetNumber is between 0 and 200, letter is between A-D.</returns>
        protected static string GetStreetNumber()
        {
            string streetNumberString = string.Empty;
            Random streetRandomize = new Random();
            int streetNumber = streetRandomize.Next(199) + 1;
            char streetSubNumber = (char)streetRandomize.Next(65, 69);
            streetNumberString = $"{streetNumber}/{streetSubNumber}";
            return streetNumberString;
        }
    }
}
